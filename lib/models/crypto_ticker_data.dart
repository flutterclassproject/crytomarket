// Crytocurrency ticker data model.
// Current price, market data and price change percent in last 1 hour, 1 day and 7 days

import 'dart:core';

class CryptoTickerData {
  String id;
  String currency;
  String name;
  String logoUrl;

  PriceData priceData;
  MarketData marketData;

  IntervalData oneHourIntervalData;
  IntervalData oneDayIntervalData;
  IntervalData sevenDayIntervalData;

  CryptoTickerData(
      {
        this.id = '',
        this.currency = '',
        this.name = '',
        this.logoUrl = '',
        this.priceData,
        this.marketData,
        this.oneDayIntervalData,
        this.oneHourIntervalData,
        this.sevenDayIntervalData
      }
  );

  factory CryptoTickerData.fromJson(Map<String, dynamic> json) {
    return CryptoTickerData(
      id: json["id"] ?? "0",
      currency: json["currency"] ?? "0",
      name: json["name"] ?? "0",
      logoUrl: json["logo_url"] ?? "0",
      priceData: PriceData.fromJson(json),
      marketData: MarketData.fromJson(json),
      oneHourIntervalData: IntervalData.fromJson(json["1h"]??{}),
      oneDayIntervalData: IntervalData.fromJson(json["1d"]??{}),
      sevenDayIntervalData: IntervalData.fromJson(json["7d"]??{})
    );
  }
}


class PriceData {
  String price;
  DateTime priceDate;

  PriceData({
    this.price = "0",
    this.priceDate
  });

  factory PriceData.fromJson(Map<String, dynamic> json) {
    return PriceData(
      price: json['price'].toString(),
      priceDate: DateTime.parse(json['price_date'].toString())
    );
  }
}

class MarketData {
  String circulatingSupply;
  String maxSupply;
  String marketCap;

  MarketData({
    this.circulatingSupply = "0",
    this.maxSupply = "0",
    this.marketCap = "0"
  });

  factory MarketData.fromJson(Map<String, dynamic> json) {
    return MarketData(
      circulatingSupply: json['circulating_supply'] ?? "0",
      maxSupply: json['max_supply'] ?? "0",
      marketCap: json['market_cap'] ?? "0",
    );
  }
}

class IntervalData {
  String priceChange;
  String priceChangePercent;

  IntervalData({
    this.priceChange = "0",
    this.priceChangePercent = "0"
  });

  factory IntervalData.fromJson(Map<String, dynamic> json) {
    return IntervalData(
      priceChange: json['price_change'] ?? "0",
      priceChangePercent: json['price_change_pct'] ?? "0"
    );
  }
}