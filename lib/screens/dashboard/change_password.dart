import 'package:flutter/material.dart';

import 'package:crytomarket/services/firebase.dart';
import 'package:crytomarket/models/user.dart';

import 'package:crytomarket/screens/utils/utils.dart';

class PasswordResetPage extends StatelessWidget {

  final FireBaseService fireBaseService = FireBaseService.instance;
  final CryptoUser usr = CryptoUser.instance;

  @override
  Widget build(BuildContext context) {
    Future<void> sendPwdResetEmail = fireBaseService.sendPasswordResetEmail(usr.email);
    return FutureBuilder<void>(
      future: sendPwdResetEmail,
      builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
        Widget passwordResetScreen;
        if(snapshot.connectionState == ConnectionState.done) {
          passwordResetScreen = Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Password reset email is sent to ${usr.email}'
              ),
              Container(
                margin: new EdgeInsets.symmetric(vertical: 20),
                child: getRaisedButton(
                  'Go to dashboard',
                  () => Navigator.of(context).popUntil((route) => route.isFirst)
                ),
              ),
            ],
          );
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          passwordResetScreen =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to send password rest link'
          );
        }
        else {
          passwordResetScreen =  getScreenCircularLoader();
        }
        return Scaffold(
          appBar: getAppBar('Password reset'),
          body: Center(
              child: passwordResetScreen
          )
        );
      },
    );
  }
}