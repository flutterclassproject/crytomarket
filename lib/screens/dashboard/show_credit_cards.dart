import 'package:flutter/material.dart';
import 'package:credit_card_slider/card_background.dart';
import 'package:credit_card_slider/card_network_type.dart';
import 'package:credit_card_slider/credit_card_widget.dart';
import 'package:credit_card_slider/validity.dart';

import 'package:crytomarket/models/user.dart';
import 'package:crytomarket/models/payment_method.dart';

import 'package:crytomarket/screens/dialogs/dialog.dart';
import 'package:crytomarket/screens/registration/add_credit_card.dart';
import 'package:crytomarket/screens/utils/utils.dart';

import 'package:crytomarket/services/stripe.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyCreditCards extends StatefulWidget {
  @override
  _MyCreditCardsState createState() => _MyCreditCardsState();
}

class _MyCreditCardsState extends State<MyCreditCards> {
  List<PaymentMethod> paymentMethods = CryptoUser.instance.paymentMethods;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {

    var onConfirmDeleteCard = (String paymentMethodId) async {
      Stripe stripe = Stripe.instance;
      print(paymentMethodId);
      try {
        Dialogs.showLoadingDialog(context, _keyLoader);
        await stripe.deletePaymentMethod(paymentMethodId);
        CryptoUser.instance.deletePaymentMethod(paymentMethodId);
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        setState(() {
          paymentMethods = [...CryptoUser.instance.paymentMethods];
        });
        await Dialogs.getAlertBox(
            context,
            'Success',
            'Your card is deleted!!',
            'Ok'
        );
      }
      catch(e) {
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        Dialogs.getAlertBox(context, 'Failed to delete card', e.toString() ?? 'Empty error message');
      }
    };

    var getEmptyCreditCardWidget = () {
      return Text(
        "You didn't register a credit card. \n Click on + to add a new card",
        style: TextStyle(
          fontSize: 20
        ),
      );
    };

    return Scaffold(
      appBar: getAppBar('Credit cards'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: paymentMethods.length == 0 ? getEmptyCreditCardWidget() : ListView.separated(
            itemBuilder: (context, index) {
              PaymentMethod paymentMethod = paymentMethods[index];
              return ListTile(
                title: CardWidget(paymentMethod.card),
                trailing: IconButton(
                  icon: Icon(
                    Icons.delete_forever,
                    size: 30,
                  ),
                  color: Colors.redAccent,
                  onPressed: () async {
                    ConfirmationStatus status =  await Dialogs.getConfirmationAlertBox(
                      context,
                      'Confirmation',
                      'Do you want to delete the card?',
                    );
                    if(status == ConfirmationStatus.CONFIRMED) {
                      onConfirmDeleteCard(paymentMethod.paymentMethodId);
                    }
                  },
                ),
              );
            },
            separatorBuilder:
                (BuildContext context, int index) =>
                    Divider(
                      height: 30,
                      thickness: 1,
                    ),
            itemCount: paymentMethods.length
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: new Color.fromRGBO(22, 82, 250, 1),
        splashColor: Colors.white,
        onPressed: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddCreditCardScreen()),
          );
          setState(() {
            paymentMethods = [...CryptoUser.instance.paymentMethods];
          });
        },
        child: Icon(FontAwesomeIcons.plus),
      ),
    );
  }
}


class CardWidget extends StatelessWidget {

  final CustomerCreditCard creditCard;

  CardWidget(this.creditCard);


  CardNetworkType getCardBrand(String brand) {
    String brandLowerCase = brand.toLowerCase();
    switch(brandLowerCase) {
      case 'visa':
        return CardNetworkType.visa;
      case 'mastercard':
        return CardNetworkType.mastercard;
      case 'americanexpress':
        return CardNetworkType.americanExpress;
      case 'discover':
        return CardNetworkType.discover;
    }
  }

  @override
  Widget build(BuildContext context) {

    String cardNumber = 'XXXXXXXXXXXX${creditCard.lastFourDigits}';
    CryptoUser user = CryptoUser.instance;

    Color getRandomColor() {
      return (Colors.primaries.toList()..shuffle()).first;
    }

    return CreditCard(
      cardBackground: GradientCardBackground(
        LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [getRandomColor(), getRandomColor()],
          stops: [0.3, 0.95],
        )
      ),
      cardNetworkType: getCardBrand(creditCard.brand),
      cardHolderName: user.name,
      cardNumber: cardNumber,
      validity: Validity(
        validThruMonth: creditCard.expMonth,
        validThruYear: creditCard.expYear,
      ),
    );
  }
}