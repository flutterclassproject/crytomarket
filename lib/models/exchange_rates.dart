import 'dart:core';

class CryptoExchangeRate {
  String currency;
  String rate;
  DateTime timestamp;

  CryptoExchangeRate({
    this.currency,
    this.rate,
    this.timestamp
  });

  factory CryptoExchangeRate.fromJson(Map<String, dynamic> json) {
    return CryptoExchangeRate(
      currency: json['currency'].toString(),
      rate: json['rate'].toString(),
      timestamp: DateTime.parse(json['timestamp'].toString())
    );
  }
}