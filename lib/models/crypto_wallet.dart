// Cryptowallet model and properties

class CryptoWallet {

  // Modify this list to add/delete support of a crypto currency.
  static List<String> getSupportedCryptos() {
    return [
      "BTC",
      "ETH",
      "LTC",
      "XRP",
      "BCH",
      "XMR",
      "EOS",
      "BNB",
      "XLM",
      "NEO"
    ];
  }

  // Empty wallet to initialize for a new customer
  static Map<String, String> getEmptyCryptoWallet() {
    return Map.fromIterable(CryptoWallet.getSupportedCryptos(), key: (crypto) => crypto, value: (e) => "0.0000000");
  }

  // Fetch price data in ticker model for these supported intervals
  static List<String> getSupportedIntervals() {
    return [
      "1h",
      "1d",
      "7d"
    ];
  }

  // This is the precision of the crypto coin count stored in the wallet.
  static int getFractionDigitsForCryptoUnitCount() {
    return 7;
  }
}