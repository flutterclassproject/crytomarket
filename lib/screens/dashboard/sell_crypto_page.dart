import 'package:flutter/material.dart';
import 'package:numeric_keyboard/numeric_keyboard.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:crytomarket/screens/dialogs/dialog.dart';
import 'package:crytomarket/screens/utils/utils.dart';
import 'package:crytomarket/screens/dashboard/confirm_sell_cryptos.dart';

import 'package:crytomarket/models/user.dart';
import 'package:crytomarket/models/crypto_ticker_data.dart';

import 'package:crytomarket/services/nomics.dart';

class CryptoSellScreen extends StatefulWidget {
  final String cryptoCurrencyName;

  CryptoSellScreen({Key key, @required this.cryptoCurrencyName}) : super(key: key);

  @override
  _CryptoSellScreenState createState() => _CryptoSellScreenState();
}

class _CryptoSellScreenState extends State<CryptoSellScreen> {
  String sellAmount = '';

  _onKeyboardTap(String value) {
    setState(() {
      sellAmount = sellAmount + value;
    });
  }

  @override
  Widget build(BuildContext context) {

    var onClickSell = () async {
      try {
        Nomics nomics = Nomics.instance;
        List<dynamic> tickerDataJSON = await nomics.getCurrenciesTickData([widget.cryptoCurrencyName]);
        CryptoTickerData currencyTickerData = CryptoTickerData.fromJson(tickerDataJSON[0]);
        String currentCryptoPricePerUnit = currencyTickerData.priceData.price;
        String currentWalletCryptoCount = CryptoUser.instance.cryptoWallet[widget.cryptoCurrencyName];

        String walletCryptoPrice = calculateCoinsPrice(currentWalletCryptoCount, currentCryptoPricePerUnit);
        String requestedCryptoSellPrice = double.parse(sellAmount).toStringAsFixed(2);

        if(double.parse(walletCryptoPrice) < double.parse(requestedCryptoSellPrice)) {
          Dialogs.getAlertBox(
              context,
              'Insufficient crypto balance', 'Your entered amount \$$requestedCryptoSellPrice is greater than your ${widget.cryptoCurrencyName} wallet balance \$$walletCryptoPrice'
          );
        }
        else{
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => ConfirmSellScreen(
              cryptoCurrencyName: widget.cryptoCurrencyName,
              sellAmount: requestedCryptoSellPrice,
              cryptoPricePerUnit: currentCryptoPricePerUnit,
            ),
          ));
        }
      }
      catch(e) {
        Dialogs.getAlertBox(
            context,
            'Error', e.toString()
        );
      }
    };

    return Scaffold(
      appBar: getAppBar('Sell now'),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                '\$ $sellAmount',
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 50
                ),
              ),
              NumericKeyboard(
                onKeyboardTap: _onKeyboardTap,
                textColor: Colors.black87,
                rightButtonFn: () {
                  if(sellAmount.length > 0) {
                    setState(() {
                      sellAmount = sellAmount.substring(0, sellAmount.length - 1);
                    });
                  }
                },
                rightIcon: Icon(
                  Icons.backspace,
                  color: Colors.black87,
                ),
                leftButtonFn: () {
                  setState(() {
                    if(sellAmount.indexOf('.') == -1) {
                      sellAmount = sellAmount+'.';
                    }
                  });
                },
                leftIcon: Icon(
                  FontAwesomeIcons.solidCircle,
                  size: 10,
                  color: Colors.black87,
                ),
              ),
              getRaisedButton(
                'Preview Sell',
                () {
                  if(sellAmount.length == 0) {
                    Dialogs.getAlertBox(context, 'Warning', 'Please enter an amount');
                    return;
                  }
                  onClickSell();
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}