// Payment method model for a credit card

class PaymentMethod {
  String paymentMethodId = ''; // Stripe's payment method id assigned to this credit card
  Map<String, dynamic> billingAddress = {};
  CustomerCreditCard card;

  PaymentMethod({this.paymentMethodId, this.billingAddress, this.card});

  factory PaymentMethod.fromJson(Map<String, dynamic> json) {
    return PaymentMethod(
      paymentMethodId: json["id"] ?? '',
      billingAddress: {
        ...json["billing_details"]
      },
      card: CustomerCreditCard.fromJson(json['card']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'paymentMethodId': paymentMethodId,
      'billing_details': billingAddress,
      'card': card.toJson()
    };
  }
}


class CustomerCreditCard {
  String brand; // Visa, Mastercard, Discover etc
  String country;
  int expMonth;
  int expYear;
  String lastFourDigits; // Last four digits of card number

  CustomerCreditCard({
    this.brand,
    this.country,
    this.expYear,
    this.expMonth,
    this.lastFourDigits
  });

  factory CustomerCreditCard.fromJson(Map<String, dynamic> json) {
    return CustomerCreditCard(
      brand: json['brand'].toString() ?? '',
      country: json['country'].toString(),
      expMonth: int.parse(json['exp_month'].toString()),
      expYear: int.parse(json['exp_year'].toString()),
      lastFourDigits: json['last4'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'brand': brand,
      'exp_month': expMonth,
      'exp_year': expYear,
      'lastFourDigits': lastFourDigits
    };
  }
}