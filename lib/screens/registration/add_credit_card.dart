import 'package:crytomarket/screens/dashboard/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';

import 'package:crytomarket/services/stripe.dart';

import 'package:crytomarket/screens/dialogs/dialog.dart';
import 'package:crytomarket/screens/utils/utils.dart';

class AddCreditCardScreen extends StatefulWidget {
  // From page to handle navigation different in registration flow.
  // There should a more graceful way to handle in navigation package to know the previous screen. Need to explore..
  final String fromPage;

  AddCreditCardScreen({
    this.fromPage = ''
  });

  @override
  State<StatefulWidget> createState() {
    return AddCreditCardScreenState();
  }
}

class AddCreditCardScreenState extends State<AddCreditCardScreen> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {

    var goToNextPage = () {
      if(widget.fromPage == "Registration") {
        // If in registration flow, and take user to dashboard
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => Dashboard()),
        );
      }
      else {
        // Just pop if not in registration flow
        Navigator.pop(context);
      }
    };

    var onSubmit = () async {
      List<String> expiry = expiryDate.split('/');
      Stripe stripe = Stripe.instance;
      try {
        Dialogs.showLoadingDialog(context, _keyLoader);
        await stripe.addCreditCard(
            cardNumber, expiry[0], '20' + expiry[1], cvvCode);
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        goToNextPage();
      }
      catch(e) {
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        await Dialogs.getAlertBox(context, 'Failed', e.toString() ?? 'Empty error message');
        goToNextPage();
      }
    };

    var onSkipAction = () {
      goToNextPage();
    };

    return Scaffold(
        appBar: getAppBar('Add your credit card'),
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: Column(
            children: <Widget>[
              CreditCardWidget(
                cardNumber: cardNumber,
                expiryDate: expiryDate,
                cardHolderName: cardHolderName,
                cvvCode: cvvCode,
                showBackView: isCvvFocused,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: CreditCardForm(
                    onCreditCardModelChange: onCreditCardModelChange,
                    onCreditCardSubmit: onSubmit,
                    onSkip: onSkipAction,
                  ),
                ),
              )
            ],
          ),
        ),
      );
  }

  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
