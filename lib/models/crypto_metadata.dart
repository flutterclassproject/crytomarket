// Cryptocurrency metadata model

import 'dart:core';

class CryptoMetaData {
  String id;
  String originalSymbol;
  String name;
  String description;
  String websiteUrl;
  String logoUrl;
  String redditUrl;
  String whitepaperUrl;
  String facebookUrl;
  String mediumUrl;
  String twitterUrl;

  CryptoMetaData(
    {
      this.id = '',
      this.originalSymbol = '',
      this.name = '',
      this.logoUrl = '',
      this.description = '',
      this.redditUrl = '',
      this.websiteUrl = '',
      this.whitepaperUrl = '',
      this.facebookUrl = '',
      this.mediumUrl = '',
      this.twitterUrl = ''
    }
  );

  factory CryptoMetaData.fromJson(Map<String, dynamic> json) {
    return CryptoMetaData(
      id: json["id"].toString(),
      originalSymbol: json["currency"].toString(),
      name: json["name"].toString(),
      description: json["description"].toString(),
      logoUrl: json["logo_url"].toString(),
      redditUrl: json["reddit_url"].toString(),
      websiteUrl: json["website_url"].toString(),
      whitepaperUrl: json["whitepaper_url"].toString(),
      facebookUrl: json["facebook_url"].toString(),
      mediumUrl: json["medium_url"].toString(),
      twitterUrl: json["twitter_url"].toString()
    );
  }
}