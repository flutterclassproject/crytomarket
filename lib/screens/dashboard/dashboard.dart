import 'package:flutter/material.dart';
import 'package:crytomarket/screens/dashboard/home.dart';
import 'package:crytomarket/screens/dashboard/portfolio.dart';
import 'package:crytomarket/screens/dashboard/prices.dart';
import 'package:crytomarket/screens/dashboard/account.dart';

import 'package:crytomarket/providers/crypto_favorites_provider.dart';
import 'package:provider/provider.dart';

import 'package:crytomarket/models/user.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    Home(),
    Portfolio(),
    Prices(),
    Account()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CryptoFavoritesModel(CryptoUser.instance.favorites),
      child: MaterialApp(
        home: Scaffold(
          body: _widgetOptions.elementAt(_selectedIndex),
          bottomNavigationBar: BottomNavigationBar(
            selectedItemColor: new Color.fromRGBO(22, 82, 250, 1),
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('Home'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.pie_chart),
                title: Text('Portfolio'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.attach_money),
                title: Text('Prices'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_box),
                title: Text('Account'),
              ),
            ],
            currentIndex: _selectedIndex,
            onTap: _onItemTapped,
          ),
        ),
      )
    );
  }
}