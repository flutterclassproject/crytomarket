import 'package:crytomarket/screens/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import 'package:crytomarket/models/user.dart';
import 'package:crytomarket/screens/dialogs/dialog.dart';
import 'package:crytomarket/screens/dashboard/dashboard.dart';
import 'package:crytomarket/screens/registration/basic_info.dart';

import 'package:crytomarket/services/firebase.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    var loginToFireBase = () async {
      try {
        Dialogs.showLoadingDialog(context, _keyLoader, "Logging in....");
        FireBaseService fireBaseService = FireBaseService.instance;
        UserCredential result = await fireBaseService.loginWithEmailAndPassword(emailController.text, passwordController.text);
        DataSnapshot snapshot = await fireBaseService.getUserProfile(result.user.uid.toString());
        CryptoUser user = CryptoUser.instance;
        user.initUserProfileFromFireBaseSnapshot(result.user.uid, snapshot.value);
        await user.loadPaymentMethodsFromStripe();
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Dashboard()));
      }
      catch(e) {
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        Dialogs.getAlertBox(context, 'Failed to login', e.toString() ?? 'Empty error message');
      }
    };

    return Scaffold(
        appBar: getAppBar('Login'),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(
                'logo.png',
                width: 200.0,
                height: 200.0,
                fit: BoxFit.cover,
              ),
              Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: "Email eg:- jondoe@gmail.com",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        // The validator receives the text that the user has entered.
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter an email Address';
                          } else if (!isEmailValidFormat(value)) {
                            return 'Invalid email address';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: TextFormField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                          labelText: "Password",
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                        // The validator receives the text that the user has entered.
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Password';
                          }
                          return null;
                        },
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width/2,
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: new Color.fromRGBO(22, 82, 250, 1),
                        child: Text(
                          'Login',
                          style: TextStyle(
                            fontSize: 20
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            loginToFireBase();
                          }
                        },
                      )
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 20),
                      child: Row(
                        children: <Widget>[
                          FlatButton(
                            textColor: new Color.fromRGBO(22, 82, 250, 1),
                            child: Text(
                              'Sign in',
                              style: TextStyle(fontSize: 20),
                            ),
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(builder: (context) => BasicInfo()),
                              );
                            },
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      )
                    )
                  ])),
            ],
          ),
        ));
  }


  bool isEmailValidFormat(String email) {
    if (email.length == 0) {
      return false;
    }
    RegExp regex = new RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    return regex.hasMatch(email);
  }

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }
}