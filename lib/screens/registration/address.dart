import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

import 'package:crytomarket/services/usa_states_cities.dart';
import 'package:crytomarket/models/user.dart';

import 'package:crytomarket/screens/registration/stripe_consent.dart';
import 'package:crytomarket/screens/utils/utils.dart';

class AddressInfo extends StatefulWidget {
  @override
  _AddressInfoState createState() => _AddressInfoState();
}

class _AddressInfoState extends State<AddressInfo> {
  bool isLoading = false;
  final _formKey = GlobalKey<FormState>();
  DatabaseReference dbRef = FirebaseDatabase.instance.reference().child("Users");
  TextEditingController line1Controller = TextEditingController();
  TextEditingController line2Controller = TextEditingController();
  TextEditingController cityController = TextEditingController();
  String selectedState;
  String selectedCity;
  TextEditingController postalCodeController = TextEditingController();

  UsaStatesAndCitiesAPI usaStateCitiesHandle = new UsaStatesAndCitiesAPI();

  List<String> states = [];
  List<String> cities = [];

  Future<Null> fetchStates() async {
    await usaStateCitiesHandle.initAPI();
    List<String> fetchedStates = await usaStateCitiesHandle.getStates();
    List<String> fetchedCities = await usaStateCitiesHandle.getCities(fetchedStates[0]);
    setState(() {
      states = fetchedStates;
      cities = fetchedCities;
      selectedState = states[0];
      selectedCity = cities[0];
    });
  }

  @override
  void initState() {
    super.initState();
    fetchStates();
  }


  Widget buildStatesDropDownWidget() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: DropdownButton<String>(
        isExpanded: true,
        hint: Text('Loading states..'),
        value: selectedState,
        icon: Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        onChanged: (String newValue) async {
          setState(() {
            selectedState = newValue;
            selectedCity = null;
          });
          List<String> fetchedCities = await usaStateCitiesHandle.getCities(newValue);
          setState(() {
            cities = fetchedCities;
            selectedCity = cities[0];
          });
        },
        items: states
            .map<DropdownMenuItem<String>>((String state) {
          return DropdownMenuItem<String>(
            value: state,
            child: Text(state),
          );
        }).toList(),
      ),
    );
  }

  Widget buildCitiesDropDownWidget() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: DropdownButton<String>(
        isExpanded: true,
        hint: Text('Loading cities..'),
        value: selectedCity,
        icon: Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        onChanged: (String newValue) {
          setState(() {
            selectedCity = newValue;
          });
        },
        items: cities
            .map<DropdownMenuItem<String>>((String city) {
          return DropdownMenuItem<String>(
            value: city,
            child: Text(city),
          );
        }).toList(),
      ),
    );
  }

  void uploadUserAddressToFireBase() {
    CryptoUser usr = CryptoUser.instance;
    dbRef.child(usr.fireBaseUUID).update({
      "address": {
        "line1": line1Controller.text,
        "line2": line2Controller.text,
        "postal_code": postalCodeController.text,
        "state": selectedState,
        "city": selectedCity,
        "country": "US"
      }
    }).then((res) {
      usr.initAddress(line1Controller.text, line2Controller.text, selectedCity, postalCodeController.text, selectedState);
      setState(() {
        isLoading = false;
      });
    }).catchError((err) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text(err.message),
              actions: [
                FlatButton(
                  child: Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                    setState(() {
                      isLoading = false;
                    });
                  },
                )
              ],
            );
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar('Address'),
      body: Center(
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: TextFormField(
                    controller: line1Controller,
                    decoration: InputDecoration(
                      labelText: "Address line 1 eg:- 1234 Norman St",
                    ),
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Enter your address street';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: TextFormField(
                    controller: line2Controller,
                    decoration: InputDecoration(
                      labelText: "Address line 2 eg:- Apt - 6",
                    ),
                  ),
                ),
                buildStatesDropDownWidget(),
                buildCitiesDropDownWidget(),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: postalCodeController,
                    decoration: InputDecoration(
                      labelText: "Postal eg:- 92115",
                    ),
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Enter your postal code';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: isLoading
                      ? CircularProgressIndicator()
                      : getRaisedButton(
                      'Next',
                        () {
                      if (_formKey.currentState.validate()) {
                        setState(() {
                          isLoading = true;
                        });
                        uploadUserAddressToFireBase();
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => StripeConsent(
                            fromPage: 'Registration',
                          )),
                        );
                      }
                    }
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}