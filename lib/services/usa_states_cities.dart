// API to fetch USA states and cities

import 'dart:core';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

class UsaStatesAndCitiesAPI {
  final String email = 'test1234@gmail.com';
  final String apiToken = 'cKITul3KvShDpu3ccAb_4HSb3a5xs16PUHy74pE2V-O0d_J8WfLq32jXGmEgr7-ynx4';
  String authToken = '';
  DateTime lastTokenFetchedTime = DateTime(2000, 01, 01); // Some past date to refresh token when requested first time
  final int tokenExpiryHours = 24;
  List<String> states = [];
  Map<String, List<String>> stateToCities = {}; // Store list of cities in local cache after fetching for first time.

  Future<void> initAPI() async {
    Response response = await get(
        'https://www.universal-tutorial.com/api/getaccesstoken',
        headers: {
          'Accept': 'application/json',
          'api-token': apiToken,
          'user-email': email
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    String fetchedAuthToken = responseData['auth_token'].toString() ?? '';
    if (response.statusCode == 200 && fetchedAuthToken.length > 0) {
      authToken = fetchedAuthToken;
      lastTokenFetchedTime = DateTime.now();
    }
  }


  bool isAuthTokenValid() {
    Duration difference = DateTime.now().difference(lastTokenFetchedTime);
    return authToken.length > 0 && difference.inHours > tokenExpiryHours;
  }

  List<String> dynamicListToStringList(List<dynamic> dList) {
    List<String> sList = [];
    dList.forEach((hashTag) => sList.add(hashTag.toString()));
    return sList;
  }

  Future<List<String>> getStates() async {
    try {
      Response response = await get(
          'https://www.universal-tutorial.com/api/states/United States',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $authToken',
          }
      );

      final List<dynamic> responseData = json.decode(response.body);
      if(response.statusCode == 200) {
        responseData.forEach((stateObject) {
          states.add(stateObject['state_name'].toString());
        });
      }
    }
    catch(e) {
      print("Failed to fetch states");
    }
    return states;
  }

  Future<List<String>> getCities(String state) async {

    if(stateToCities.containsKey(state) && stateToCities[state].length > 0) {
      print(stateToCities[state]);
      return stateToCities[state];
    }

    if(this.isAuthTokenValid()) {
      this.initAPI();
    }

    List<String> cities = [];
    try {
      Response response = await get(
          'https://www.universal-tutorial.com/api/cities/$state',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $authToken',
          }
      );
      final List<dynamic> responseData = json.decode(response.body);
      if(response.statusCode == 200) {
        responseData.forEach((cityObject) {
          cities.add(cityObject['city_name'].toString());
        });
      }
    }
    catch(e) {
      print("Failed to fetch cities for state $state");
    }
    stateToCities[state] = cities;
    return cities;
  }
}