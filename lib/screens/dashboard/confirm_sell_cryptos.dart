import 'package:flutter/material.dart';

import 'package:crytomarket/screens/utils/utils.dart';
import 'package:crytomarket/screens/dialogs/dialog.dart';

import 'package:crytomarket/services/stripe.dart';
import 'package:crytomarket/models/user.dart';

class ConfirmSellScreen extends StatefulWidget {

  final String sellAmount;
  final String cryptoCurrencyName;
  final String cryptoPricePerUnit;

  ConfirmSellScreen({
    Key key,
    @required this.sellAmount,
    @required this.cryptoCurrencyName,
    @required this.cryptoPricePerUnit
  }) : super(key: key);

  @override
  _ConfirmSellScreenState createState() => _ConfirmSellScreenState();
}

class _ConfirmSellScreenState extends State<ConfirmSellScreen> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String numberCryptoUnitsToSell = '';

  @override
  void initState() {
    numberCryptoUnitsToSell = getNumberCryptoUnitsForPayment(widget.sellAmount, widget.cryptoPricePerUnit);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var getSoldCryptoCountForSellingPrice = () {
      return Column(
        children: [
          Text(
            "You are selling",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 30,
                color: Colors.black87
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "$numberCryptoUnitsToSell ${widget.cryptoCurrencyName}",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 30,
                color: Colors.black87
            ),
          ),
          Text(
            "=",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 30,
                color: Colors.black87
            ),
          ),
          Text(
            "\$${widget.sellAmount}",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 30,
                color: Colors.black87
            ),
          ),
        ],
      );
    };

    var sellCryptoCreditSummary = () {
      double containerHeight = MediaQuery.of(context).size.height/15;
      const textStyle = TextStyle(
          color: Colors.black87,
          fontSize: 20
      );

      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: containerHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${widget.cryptoCurrencyName} price per unit',
                  style: textStyle,
                ),
                Text(
                  adjustPrice(widget.cryptoPricePerUnit),
                  style: textStyle,
                )
              ],
            ),
          ),
          Container(
            height: containerHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Credited to your account',
                  style: textStyle,
                ),
                Text(
                  '\$${widget.sellAmount}',
                  style: textStyle,
                )
              ],
            ),
          ),
        ],
      );
    };

    var onConfirmSell = (String transactionId) async {
      Stripe stripe = Stripe.instance;
      try {
        Dialogs.showLoadingDialog(context, _keyLoader);
        await stripe.confirmCreditTransaction(transactionId);
        await CryptoUser.instance.decreaseCryptoWalletBalance(widget.cryptoCurrencyName, numberCryptoUnitsToSell);
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        await Dialogs.getAlertBox(
            context,
            'Success',
            '\$${widget.sellAmount} is credited to your Stripe account',
            'Go to Dashboard'
        );
        Navigator.of(context).popUntil((route) => route.isFirst);
      }
      catch(e) {
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        Dialogs.getAlertBox(context, 'Failed to process payment', e.toString() ?? 'Empty error message');
      }
    };

    var onClickSell = () async {
      Stripe stripe = Stripe.instance;
      try {
        int creditAmountInCents = (double.parse(widget.sellAmount)*100).floor();
        Dialogs.showLoadingDialog(context, _keyLoader);
        Map<String, String> metaData = {
          'cryptoCurrency': widget.cryptoCurrencyName,
          'coinCount': numberCryptoUnitsToSell
        };
        Map<String, dynamic> response = await stripe.createCreditAmountTransaction(creditAmountInCents, metaData);
        Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
        ConfirmationStatus status = await Dialogs.getConfirmationAlertBox(context,
            'Confirmation',
            'Do you confirm to sell $numberCryptoUnitsToSell ${widget.cryptoCurrencyName}?',
        );
        if(status == ConfirmationStatus.CONFIRMED) {
          onConfirmSell(response['id'].toString());
        }
      }
      catch(e) {
        Dialogs.getAlertBox(context, 'Failed', e.toString() ?? 'Empty error message');
      }
    };

    var confirmSellButton = () {
      return getRaisedButton(
        'Sell',
        onClickSell
      );
    };

    return  Scaffold(
      appBar: getAppBar('Sell summary'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              getSoldCryptoCountForSellingPrice(),
              sellCryptoCreditSummary(),
              confirmSellButton()
            ],
          ),
        )
      )
    );
  }
}