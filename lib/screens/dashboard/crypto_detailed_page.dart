import 'package:crytomarket/models/crypto_metadata.dart';
import 'package:crytomarket/models/user.dart';
import 'package:flutter/material.dart';

import 'package:crytomarket/services/nomics.dart';

import 'package:provider/provider.dart';
import 'package:crytomarket/providers/crypto_metadata_provider.dart';
import 'package:crytomarket/providers/crypto_ticker_provider.dart';
import 'package:crytomarket/providers/crypto_sparkline_provider.dart';
import 'package:crytomarket/providers/crypto_favorites_provider.dart';

import 'package:crytomarket/models/crypto_ticker_data.dart';
import 'package:crytomarket/models/crypto_price_sparkline.dart';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:intl/intl.dart';
import 'package:crytomarket/screens/utils/utils.dart';
import 'package:crytomarket/screens/dashboard/buy_crypto_page.dart';
import 'package:crytomarket/screens/dashboard/sell_crypto_page.dart';
import 'package:crytomarket/screens/registration/stripe_consent.dart';

class CryptoDetailedPage extends StatefulWidget {

  final String cryptoCurrencyName;

  CryptoDetailedPage({Key key, @required this.cryptoCurrencyName}) : super(key: key);

  @override
  _CryptoDetailedPageState createState() => _CryptoDetailedPageState();
}

class _CryptoDetailedPageState extends State<CryptoDetailedPage> {
  String endTime, startTime;

  @override
  initState() {
    super.initState();
    DateTime nowUTCTime = new DateTime.now().toUtc();
    endTime = nowUTCTime.toIso8601String();
    startTime = nowUTCTime.subtract(
        new Duration(
            hours: CryptoPriceSparkLineData.sparLineDataWindowInHours
        )
    ).toIso8601String();
  }

  @override
  Widget build(BuildContext context) {
    String currency = widget.cryptoCurrencyName;
    Future<List<dynamic>> _fetchCurrencyExchangeData = Nomics.instance.getCurrenciesTickData([currency]);
    Future<List<dynamic>> _fetchCurrencyMetaData = Nomics.instance.getCryptoMetaData([currency]);
    Future<List<dynamic>> _fetchCurrencySparkLineData = Nomics.instance.getCryptoSparkLineData([currency], startTime, endTime);

    return FutureBuilder<List<dynamic>>(
      future: Future.wait([
        _fetchCurrencyExchangeData,
        _fetchCurrencyMetaData,
        _fetchCurrencySparkLineData
      ]),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        Widget detailedCryptoWidget;
        if(snapshot.hasData) {
          detailedCryptoWidget =  MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (context) => CryptoTickerDataModel(snapshot.data[0])),
              ChangeNotifierProvider(create: (context) => CryptoMetaDataModel(snapshot.data[1])),
              ChangeNotifierProvider(create: (context) => CryptoSparkLineDataModel(snapshot.data[2])),
            ],
            child: CryptoDetailsScreen(),
          );
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          detailedCryptoWidget =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to load crypto prices - ${snapshot.error.toString() ?? ''}'
          );
        }
        else {
          detailedCryptoWidget = getScreenCircularLoader();
        }
        return Scaffold(
            appBar: getAppBar('Crypto detailed page'),
            body: Center(
              child: detailedCryptoWidget
            )
        );
      }
    );
  }
}

class PricesHeader extends StatelessWidget {

  Widget getPriceDataWidget(String priceChangePercent, [String prefix, TextStyle prefixStyle, TextStyle priceChangeStyle]) {
    return Row(
      children: [
        Text(
          '$prefix',
          style: prefixStyle,
        ),
        SizedBox(
          width: 10,
        ),
        Row(
            children: <Widget>[
              getArrow(priceChangePercent),
              Text(
                '$priceChangePercent%',
                style: priceChangeStyle
              )
            ]
        ),
      ],
    );
  }

  Widget getOneHourPriceDetails(PriceData priceData, IntervalData intervalData) {
    return getPriceDataWidget(intervalData.priceChangePercent, adjustPrice(priceData.price), TextStyle(fontSize: 35), TextStyle(
        fontSize: 25,
        color: getColorForPriceChangePercent(intervalData.priceChangePercent)
    ));
  }

  Widget getOneDayAndOneWeekPriceChangeDetails(IntervalData oneDayData, IntervalData sevenDayData) {
    return Row(
      children: [
        getPriceDataWidget(oneDayData.priceChangePercent, 'Yesterday', TextStyle(fontSize: 15), TextStyle(
            fontSize: 15,
            color: getColorForPriceChangePercent(oneDayData.priceChangePercent)
        )),
        SizedBox(
          width: 10,
        ),
        getPriceDataWidget(sevenDayData.priceChangePercent, 'Last week', TextStyle(fontSize: 15), TextStyle(
            fontSize: 15,
            color: getColorForPriceChangePercent(sevenDayData.priceChangePercent)
        )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);

    CryptoTickerData tickerData = cryptoTickerDataProvider.getAllCryptos()[0];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CryptoCoinTitleRow(),
        SizedBox(height: 10,),
        getOneHourPriceDetails(tickerData.priceData, tickerData.oneHourIntervalData),
        getOneDayAndOneWeekPriceChangeDetails(tickerData.oneDayIntervalData, tickerData.sevenDayIntervalData)
      ],
    );
  }
}

class CryptoCoinTitleRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    CryptoMetaData metaData = cryptoMetaDataProvider.getCryptoMetaData()[0];

    final cryptoFavoritesProvider = Provider.of<CryptoFavoritesModel>(context);

    String cryptoId = metaData.id;
    bool isFavorite = cryptoFavoritesProvider.getFavoriteCryptos().contains(cryptoId);

    return Row (
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          '${metaData.name} price',
          style: TextStyle(
              fontSize: 20
          ),
        ),
        Ink(
          child: IconButton(
            icon: Icon(isFavorite ?
              Icons.favorite :
              Icons.favorite_border,
            ),
            color: isFavorite ? Colors.red : null,
            onPressed: () {
              if(isFavorite) {
                cryptoFavoritesProvider.removeFromFavorites(cryptoId);
                return;
              }
              cryptoFavoritesProvider.addToFavorites(cryptoId);
            },
          ),
        ),
      ],
    ) ;
  }
}

class SparkLinePriceGraph extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoSparkPriceDataProvider = Provider.of<CryptoSparkLineDataModel>(context, listen: false);

    final List<PriceTimeSeries> sparkLineData = cryptoSparkPriceDataProvider.getPriceAsTimeSeries();

    final simpleCurrencyFormatter =
    new charts.BasicNumericTickFormatterSpec.fromNumberFormat(
        new NumberFormat.compactSimpleCurrency()
    );

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.3,
      child: new charts.TimeSeriesChart([
        new charts.Series<PriceTimeSeries, DateTime>(
          id: 'Historical Price data',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (PriceTimeSeries priceTimeSeries, _) => priceTimeSeries.time,
          measureFn: (PriceTimeSeries priceTimeSeries, _) => priceTimeSeries.price,
          data: sparkLineData,
        )
      ],
      animate: true,
      primaryMeasureAxis: new charts.NumericAxisSpec(
          tickFormatterSpec: simpleCurrencyFormatter,
          tickProviderSpec: charts.BasicNumericTickProviderSpec(zeroBound: false)
      ),
      defaultRenderer: charts.LineRendererConfig(includeArea: true),
      domainAxis: new charts.DateTimeAxisSpec(
       showAxisLine: false
      )),
    );
  }
}

class CryptWalletBalance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    CryptoTickerData tickerData = cryptoTickerDataProvider.getAllCryptos()[0];

    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    CryptoMetaData metaData = cryptoMetaDataProvider.getCryptoMetaData()[0];

    return Card(
      child: ListTile(
        leading: getLeadingWidget(metaData.id),
        title: Text('${metaData.id} Wallet'),
        trailing: showTrailngWalletBalance(metaData.id, tickerData.priceData.price)
      ),
    );
  }
}

class Trade extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    CryptoMetaData metaData = cryptoMetaDataProvider.getCryptoMetaData()[0];

    return new ButtonBar(
      alignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        getRaisedButton(
          'Buy',
          () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => CryptoBuyScreen(
                cryptoCurrencyName: metaData.id,
              ),
            ));
          }
        ),
        getRaisedButton(
          'Sell',
          () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => CryptoSellScreen(
                cryptoCurrencyName: metaData.id,
              ),
            ));
          }
        ),
      ],
    );
  }
}

class AboutCrypto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    CryptoMetaData metaData = cryptoMetaDataProvider.getCryptoMetaData()[0];

    return RichText(
      maxLines: 5,
      overflow: TextOverflow.ellipsis,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(text: 'About ${metaData.name}\n', style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.black,
            fontSize: 20
          )),
          TextSpan(
            text: metaData.description,
            style: TextStyle(
              color: Colors.black,
              fontSize: 15
            )
          ),
        ],
      ),
    );
  }
}



class Resources extends StatelessWidget {

  Widget getResourceLinkIcon([FaIcon faicon, String resourceUrl = '']) {
    resourceUrl = resourceUrl ?? '';
    return Opacity(
      opacity: resourceUrl.length > 0 ? 1.0 : 0.0,
      child: IconButton(
          icon: faicon,
          onPressed: () async {
            String url = resourceUrl;
            if (await canLaunch(url)) {
              await launch(url);
            } else {
              throw 'Could not launch $url';
            }
          }
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    CryptoMetaData metaData = cryptoMetaDataProvider.getCryptoMetaData()[0];

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            'Resources',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20
            )
        ),
        Row(
          children: [
            getResourceLinkIcon(FaIcon(FontAwesomeIcons.globe), metaData.whitepaperUrl),
            getResourceLinkIcon(FaIcon(FontAwesomeIcons.facebook), metaData.facebookUrl),
            getResourceLinkIcon(FaIcon(FontAwesomeIcons.twitter), metaData.twitterUrl),
            getResourceLinkIcon(FaIcon(FontAwesomeIcons.medium), metaData.mediumUrl),
            getResourceLinkIcon(FaIcon(FontAwesomeIcons.reddit), metaData.redditUrl)
          ],
        )
      ],
    );
  }
}

class MarketInformation extends StatelessWidget {

  Widget getIconAndMarketStatName(IconData faIcon, String marketStateName) {
    return Column(
      children: [
        Icon(faIcon),
        Text(marketStateName)
      ],
    );
  }

  Widget getMarketStatRow(IconData faIcon, String marketStatValue, String marketStateName) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Icon(faIcon),
        Text(marketStateName),
        Text(marketStatValue),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    CryptoTickerData cryptoTickerData = cryptoTickerDataProvider.getAllCryptos()[0];
    MarketData marketData = cryptoTickerData.marketData;

    final simpleFormatter = new NumberFormat.compact();
    final currencyFormatter = new NumberFormat.compactSimpleCurrency();

    String formattedMarketCap = currencyFormatter.format(double.parse(marketData.marketCap));
    String formattedVolume = currencyFormatter.format(double.parse(marketData.maxSupply));
    String formattedCirculatingSupply = simpleFormatter.format(double.parse(marketData.circulatingSupply));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            'Market stats',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20
            )
        ),
        SizedBox(height: 10,),
        getMarketStatRow(FontAwesomeIcons.chartArea, formattedMarketCap, 'Market cap'),
        SizedBox(height: 10,),
        getMarketStatRow(FontAwesomeIcons.chartBar, formattedVolume, 'Volume'),
        SizedBox(height: 10,),
        getMarketStatRow(FontAwesomeIcons.chartPie, formattedCirculatingSupply, 'Circulating Supply'),
        SizedBox(height: 10,),
      ],
    );
  }
}

class CryptoDetailsScreen extends StatefulWidget {

  @override
  _CryptoDetailsScreenState createState() => _CryptoDetailsScreenState();
}

class _CryptoDetailsScreenState extends State<CryptoDetailsScreen> {
  bool hasUserGivenStripeConsent = CryptoUser.instance.hasGivenStripeConsent();

  @override
  Widget build(BuildContext context) {

    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    CryptoMetaData metaData = cryptoMetaDataProvider.getCryptoMetaData()[0];

    var stripeConsentButton = () {
      return RaisedButton(
        child: Text(
            'Want to buy ${metaData.name}?'
        ),
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(
            builder: (context) => StripeConsent(),
          ));
          setState(() {
            hasUserGivenStripeConsent = CryptoUser.instance.hasGivenStripeConsent();
          });
        },
      );
    };

    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            PricesHeader(),
            SizedBox(height: 10),
            SparkLinePriceGraph(),
            SizedBox(height: 10),
            CryptWalletBalance(),
            SizedBox(height: 10),
            hasUserGivenStripeConsent ? Trade() : stripeConsentButton(),
            SizedBox(height: 10),
            AboutCrypto(),
            SizedBox(height: 10),
            Resources(),
            SizedBox(height: 10),
            MarketInformation()
          ],
        ),
      ),
    );
  }
}

