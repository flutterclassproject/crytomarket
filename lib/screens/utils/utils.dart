import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:core';
import 'package:crytomarket/models/user.dart';
import 'package:crytomarket/models/crypto_wallet.dart';

Widget getErrorScreen([IconData icon = Icons.error_outline, Color iconColor = Colors.red, errorMsg = 'Failed to fetch']) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Icon(
        icon,
        color: iconColor,
        size: 60,
      ),
      Padding(
        padding: const EdgeInsets.only(top: 16),
        child: Text('Error: $errorMsg'),
      )
    ],
  );
}

Widget getScreenCircularLoader([double width = 30, double height = 30]) {
  return SizedBox(
    child: CircularProgressIndicator(),
    width: width,
    height: height,
  );
}

String adjustPrice(String cryptoPrice) {
  int decimals = 2;
  int fac = pow(10, decimals);
  double d = double.parse(cryptoPrice);
  return "\$" + (d = (d * fac).round() / fac).toString();
}

Color getColorForPriceChangePercent(String priceChangePercent) {
  return double.parse(priceChangePercent).isNegative ? Colors.red : Colors.green;
}

Icon getArrow(String priceChangePercent) {
  return Icon(
    double.parse(priceChangePercent).isNegative ? Icons.arrow_drop_down : Icons.arrow_drop_up,
    color: getColorForPriceChangePercent(priceChangePercent),
  );
}

AppBar getAppBar(String title, [Color appBarColor = Colors.black87]){
  return AppBar(
    iconTheme: IconThemeData(
      color: appBarColor
    ),
    centerTitle: true,
    title: Text(
      title,
      style: TextStyle(
        color: appBarColor,
      ),
    ),
    backgroundColor: Colors.transparent,
    elevation: 0,
  );
}

CircleAvatar getLeadingWidget(String name, [Color color = Colors.white]) {
  String currencyCodeLowerCase = name.toLowerCase();
  return new CircleAvatar(
      backgroundColor: Colors.white,
      child: Image.network(
        'https://raw.githubusercontent.com/spothq/cryptocurrency-icons/master/32%402x/color/$currencyCodeLowerCase%402x.png',
      )
  );
}

String calculateCoinsPrice(String noOfCryptoCoins, String pricePerCoin) {
  double noOfCoins = double.parse(noOfCryptoCoins);
  double pricePerUnit = double.parse(pricePerCoin);
  return (noOfCoins*pricePerUnit).toStringAsFixed(2); //(4321.12345678).toStringAsFixed(3); -> 4321.123
}

Widget showTrailngWalletBalance(String currency, String price) {
  CryptoUser user = CryptoUser.instance;
  String noOfCoinsInWallet = user.cryptoWallet[currency];  // No of coins of this currency in the wallet
  String calculatedCryptoValue = calculateCoinsPrice(noOfCoinsInWallet, price);
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Text('\$$calculatedCryptoValue'),
      Text('$noOfCoinsInWallet $currency'),
    ],
  );
}

String getNumberCryptoUnitsForPayment(String payment, String cryptoPricePerUnit) {
  double parsePaymentAmount = double.parse(payment);
  double parseCryptoPricePerUnit = double.parse(cryptoPricePerUnit);
  return (parsePaymentAmount/parseCryptoPricePerUnit).toStringAsFixed(CryptoWallet.getFractionDigitsForCryptoUnitCount());
}

Widget getRaisedButton(String buttonText, [Function onClick]) {
  onClick = onClick ?? (){};
  return RaisedButton(
    padding: const EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
    textColor: Colors.white,
    splashColor: Colors.white,
    color: new Color.fromRGBO(22, 82, 250, 1),
    child: Text(
      buttonText,
      style: TextStyle(
          fontSize: 20
      ),
    ),
    shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(30.0)
    ),
    onPressed: onClick,
  );
}