import 'package:crytomarket/screens/utils/utils.dart';
import 'package:flutter/material.dart';

import 'package:crytomarket/models/user.dart';


class ProfileInfoRow extends StatelessWidget {
  final String profileProperty;
  final String profileValue;

  ProfileInfoRow({
    @required this.profileProperty,
    @required this.profileValue
  });

  @override
  Widget build(BuildContext context) {

    const TextStyle textStyle = TextStyle(
        fontSize: 15
    );

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              child: Center(
                child: Text(
                  profileProperty,
                  style: textStyle,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Center(
                child: Text(
                  profileValue,
                  style: textStyle,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Profile extends StatelessWidget {

  final CryptoUser user = CryptoUser.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar('My Profile'),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Column(
                  children: [
                    Text(
                      'Personal Information',
                      style: TextStyle(
                        fontSize: 30
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ProfileInfoRow(
                      profileProperty: 'Name',
                      profileValue: user.name,
                    ),
                    ProfileInfoRow(
                      profileProperty: 'Email',
                      profileValue: user.email,
                    ),
                    ProfileInfoRow(
                      profileProperty: 'Phone',
                      profileValue: user.phone,
                    )
                  ],
                ),
                Divider(
                  height: 100,
                  thickness: 1,
                ),
                Column(
                  children: [
                    Text(
                      'Address',
                      style: TextStyle(
                          fontSize: 30
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    ProfileInfoRow(
                      profileProperty: 'Line 1',
                      profileValue: user.address.line_1 ?? '',
                    ),
                    ProfileInfoRow(
                      profileProperty: 'Line 2',
                      profileValue: user.address.line_2 ?? '',
                    ),
                    ProfileInfoRow(
                      profileProperty: 'City',
                      profileValue: user.address.city ?? '',
                    ),
                    ProfileInfoRow(
                      profileProperty: 'State',
                      profileValue: user.address.state ?? '',
                    ),
                    ProfileInfoRow(
                      profileProperty: 'Postal code',
                      profileValue: user.address.postalCode,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}