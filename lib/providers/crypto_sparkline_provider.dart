// Provider of Crypto currencies sparkline data

import 'package:flutter/cupertino.dart';
import 'package:crytomarket/models/crypto_price_sparkline.dart';


class CryptoSparkLineDataModel extends ChangeNotifier {
  CryptoPriceSparkLineData cryptoPriceSparkLineData;

  CryptoSparkLineDataModel(List<dynamic> initCryptoSparkData) {
    cryptoPriceSparkLineData = CryptoPriceSparkLineData.fromJson(initCryptoSparkData[0]);
  }

  CryptoPriceSparkLineData getCryptoSparkLineData() {
    return cryptoPriceSparkLineData;
  }

  List<PriceTimeSeries> getPriceAsTimeSeries() {
    return cryptoPriceSparkLineData.getPriceAsTimeSeries();
  }
}