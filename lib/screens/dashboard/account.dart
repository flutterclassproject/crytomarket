import 'package:flutter/material.dart';

import 'package:crytomarket/screens/dashboard/profile.dart';
import 'package:crytomarket/screens/dashboard/show_credit_cards.dart';
import 'package:crytomarket/screens/dashboard/show_crypto_transactions.dart';
import 'package:crytomarket/screens/dashboard/change_password.dart';
import 'package:crytomarket/screens/registration/stripe_consent.dart';
import 'package:crytomarket/screens/login/login.dart';
import 'package:crytomarket/screens/utils/utils.dart';

import 'package:crytomarket/models/user.dart';

import 'package:crytomarket/services/firebase.dart';

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {

  bool hasUserGivenStripeConsent = CryptoUser.instance.hasGivenStripeConsent();

  @override
  Widget build(BuildContext context) {


    Map<String, Widget> accountSections = {
      'My Profile': Profile(),
      'Change Password': PasswordResetPage(),
    };


    if(hasUserGivenStripeConsent) {
      accountSections['Manage credit cards'] = MyCreditCards();
      accountSections['Crypto purchases'] = CryptoTransactions(TransactionType.PURCHASE);
      accountSections['Crypto sales'] = CryptoTransactions(TransactionType.SALES);
    }
    else {
      accountSections['Give Stripe consent'] = StripeConsent();
    }

    accountSections['Sign Out'] = Login();

    return Scaffold(
      appBar: getAppBar('My Account'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) => Divider(height: 35,),
            itemCount: accountSections.keys.toList().length,
            itemBuilder: (context, index) {
              if(accountSections.keys.toList()[index] == 'Sign Out') {
                return Container(
                  margin: new EdgeInsets.symmetric(horizontal: 80),
                  height: 50,
                  child: getRaisedButton(
                    'Sign out',
                    () async {
                      FireBaseService fireBaseService = FireBaseService.instance;
                      await fireBaseService.signout();
                      CryptoUser.init();
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => Login()),
                      );
                    },
                  ),
                );
              }
              return ListTile(
                title: Text(accountSections.keys.toList()[index]),
                trailing: Icon(Icons.keyboard_arrow_right),
                onTap: () async {
                  await Navigator.push(context, MaterialPageRoute(
                    builder: (context) => accountSections[accountSections.keys.toList()[index]],
                  ));
                  setState(() {
                    hasUserGivenStripeConsent = CryptoUser.instance.hasGivenStripeConsent();
                  });
                }
              );
            },
          ),
        ),
      ),
    );
  }
}