import 'package:flutter/material.dart';

import 'package:crytomarket/services/stripe.dart';

import 'package:crytomarket/screens/utils/utils.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timeago/timeago.dart' as timeago;

enum TransactionType {
  PURCHASE,
  SALES
}

class CryptoTransactions extends StatelessWidget {
  final TransactionType transactionType;

  CryptoTransactions(
      this.transactionType,
  );

  @override
  Widget build(BuildContext context) {

    Future<Map<String, dynamic>> _fetchTransactions;

    if(transactionType == TransactionType.PURCHASE) {
      _fetchTransactions = Stripe.instance.fetchCryptoBuyTransactions(2);
    }
    else {
      _fetchTransactions = Stripe.instance.fetchCryptoSellTransactions(2);
    }

    var getEmptyTransactionsWidget = () {
      return Text(
        "Your don't have any transactions yet",
        style: TextStyle(
            fontSize: 20
        ),
      );
    };

    var getTransactionStatusIcon = (String status) {
      Color iconColor = Colors.red;
      IconData statusIcon = FontAwesomeIcons.exclamationTriangle;
      switch(status) {
        case "succeeded":
          iconColor = Colors.green;
          statusIcon = FontAwesomeIcons.check;
          break;
        case "processing":
          iconColor = Colors.orange;
          statusIcon = FontAwesomeIcons.spinner;
          break;
        case "cryptoSell":
          iconColor = Colors.green;
          statusIcon = FontAwesomeIcons.moneyBillWave;
          break;
      }
      return Icon(
        statusIcon,
        color: iconColor
      );
    };

    var buildRow = (transaction) {
      String currency = transaction['metadata']['cryptoCurrency'];
      String currencyCount = transaction['metadata']['coinCount'];
      String transactionValueInUSD = (transaction['amount']/100).toStringAsFixed(2);
      String transactionDate = timeago.format(DateTime.fromMillisecondsSinceEpoch(transaction['created']*1000));

      return ListTile(
        leading: getLeadingWidget(currency),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '$currencyCount $currency for \$$transactionValueInUSD',
              textAlign: TextAlign.justify,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            Text(
              transactionDate,
            ),
          ],
        ),
        trailing: getTransactionStatusIcon(
          transactionType ==  TransactionType.SALES ? 'cryptoSell' : transaction['status']
        ),
      );
    };

    return FutureBuilder<Map<String, dynamic>>(
      future: _fetchTransactions,
      builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
        Widget transactionsList;
        if(snapshot.hasData) {
          transactionsList = getEmptyTransactionsWidget();
          if(snapshot.data['data'].length > 0) {
            transactionsList = ListView.builder(
              itemCount: snapshot.data['data'].length,
              padding:
              const EdgeInsets.all(16.0),
              itemBuilder: (context, i) {
                final index = i;
                return buildRow(snapshot.data['data'][index]); //build the row widget
              }
            );
          }
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          transactionsList =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to load crypto prices'
          );
        }
        else {
          transactionsList =  getScreenCircularLoader();
        }
        String appBarTitle = 'Your crypto purchases';
        if(transactionType == TransactionType.SALES) {
          appBarTitle = 'Your crypto sales';
        }
        return Scaffold(
          appBar: getAppBar(appBarTitle),
          body: Center(
            child: Padding(
             padding: const EdgeInsets.all(8.0),
             child: transactionsList,
            )
          )
        );
      },
    );
  }
}