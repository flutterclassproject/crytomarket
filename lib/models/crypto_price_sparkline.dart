// Cryptocurrency sparkline historical price data model

import 'dart:core';

class CryptoPriceSparkLineData {
  String currency;
  List<String> timeStamps;
  List<String> prices;

  static int sparLineDataWindowInHours = 30 * 24; // Fetch price data of last 30 days

  CryptoPriceSparkLineData(
    {
      this.currency = '',
      this.timeStamps = const [],
      this.prices = const []
    }
  );

  factory CryptoPriceSparkLineData.fromJson(Map<String, dynamic> json) {

    List<String> dynamicListToStringList(List<dynamic> dList) {
      List<String> sList = [];
      dList.forEach((hashTag) => sList.add(hashTag.toString()));
      return sList;
    }

    return CryptoPriceSparkLineData(
        currency: json["currency"],
        timeStamps: dynamicListToStringList(json["timestamps"]),
        prices: dynamicListToStringList(json["prices"])
    );
  }
  
  List<PriceTimeSeries> getPriceAsTimeSeries() {
    List<PriceTimeSeries> priceTimeSeries = [];
    this.timeStamps.asMap().forEach((int index, String timeStamp) {
      priceTimeSeries.add(
          PriceTimeSeries(
              DateTime.parse(timeStamp),
              double.parse(this.prices[index])
          )
      );
    });
    return priceTimeSeries;
  }
}

class PriceTimeSeries {
  final DateTime time;
  final double price;

  PriceTimeSeries(this.time, this.price);
}