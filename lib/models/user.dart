// Customer/App user model

import 'dart:convert';

import 'package:crytomarket/models/crypto_wallet.dart';
import 'package:crytomarket/models/payment_method.dart';

import 'package:crytomarket/services/stripe.dart';
import 'package:crytomarket/services/firebase.dart';

class CryptoUser {
  String name;
  String email;
  String phone;
  Address address;
  String fireBaseUUID;

  String stripeCustomerId = '';
  bool stripeConsent = false;

  List<PaymentMethod> paymentMethods = []; // List of credit cards

  static CryptoUser _user;

  Map<String, String> cryptoWallet = Map.fromIterable(CryptoWallet.getSupportedCryptos(), key: (e) => e, value: (e) => "0.00");

  List<String> favorites = []; // List of favorite crypto currencies of the user

  CryptoUser();

  static void init() {
    _user = new CryptoUser();
  }

  static CryptoUser get instance {
    if (_user == null) {
      throw Exception('Attempted to get singleton instance of Stripe without initialization');
    }
    return _user;
  }

  initUserProfileFromFireBaseSnapshot(String firebaseUUID, Map<dynamic, dynamic> snapshot) {
    this.name = snapshot['name'].toString();
    this.email = snapshot['email'].toString();
    this.phone = snapshot['phone'].toString();
    this.fireBaseUUID = firebaseUUID;
    this.address = Address.getAddressFromSnapshot(snapshot['address']);
    this.stripeCustomerId = snapshot['stripe_customer_id'].toString();
    this.stripeConsent = snapshot['stripe_consent'];
    if(snapshot.containsKey('wallet')) {
      snapshot['wallet'].forEach(
              (currency, currencyCoinCount) => this.cryptoWallet[currency.toString()] = currencyCoinCount.toString()
      );
    }
    if(snapshot.containsKey('favorites')) {
      this.favorites = [...snapshot['favorites']];
    }
  }

  initBasicInfo(String fireBaseUUID, String name, String email, String phone) {
    this.fireBaseUUID = fireBaseUUID;
    this.name = name;
    this.email = email;
    this.phone = phone;
  }

  initAddress(String line_1, String line_2, String city, String postalCode, String state) {
    address = Address(city, line_1, line_2, postalCode, state);
  }

  Map getAddress() {
    return this.address.getAddressAsJSON();
  }

  Map<String, dynamic> getContactDetails() {
    return {
      'email': this.email,
      'name': this.name,
      'phone': this.phone,
      'address': this.address.getAddressAsJSON()
    };
  }

  void addPaymentMethod(Map<String, dynamic> json) {
    this.paymentMethods.add(PaymentMethod.fromJson(json));
  }

  Map<String, dynamic> getPaymentMethod() {
    return this.paymentMethods[0].toJson();
  }

  bool hasGivenStripeConsent() {
    return this.stripeConsent;
  }

  String getStripeCustomerId() {
    return this.stripeCustomerId;
  }

  Future<void> loadPaymentMethodsFromStripe() async {
    if(this.hasGivenStripeConsent()) {
      Stripe stripe = Stripe.instance;
      Map<String, dynamic> paymentMethodsJson = await stripe.loadPaymentMethods();
      paymentMethodsJson['data'].forEach(
              (paymentMethod) => this.addPaymentMethod(paymentMethod)
      );
    }
  }

  // Update a crypto currency's coin count in the wallet
  Future<void> updateCryptoWalletBalance(String cryptoCurrency, double newCryptoCoinCount) async {
    Map<String, String> updatedWallet = {...{
      ...cryptoWallet,
      ...{
        cryptoCurrency: newCryptoCoinCount
            .toStringAsFixed(
            CryptoWallet.getFractionDigitsForCryptoUnitCount()
        )
      }
    }};
    await FireBaseService.instance.updateUserWallet(fireBaseUUID, updatedWallet);
    this.cryptoWallet = {
      ...updatedWallet
    };
  }

  // Increase wallet count when buying a crypto
  Future<void> increaseCryptoWalletBalance(String cryptoCurrency, String purchasedCoinCount) async {
    String cryptoCurrencyCurrentBalance = cryptoWallet[cryptoCurrency];
    double newCryptoCurrencyBalance = double.parse(cryptoCurrencyCurrentBalance) + double.parse(purchasedCoinCount);
    await updateCryptoWalletBalance(cryptoCurrency, newCryptoCurrencyBalance);
  }

  // Decrease wallet count when selling a crypto
  Future<void> decreaseCryptoWalletBalance(String cryptoCurrency, String soldCoinCount) async {
    String cryptoCurrencyCurrentBalance = cryptoWallet[cryptoCurrency];
    double newCryptoCurrencyBalance = double.parse(cryptoCurrencyCurrentBalance) - double.parse(soldCoinCount);
    await updateCryptoWalletBalance(cryptoCurrency, newCryptoCurrencyBalance);
  }

  // Update favorite cryptocurrencies
  Future<void> updateFavorites(List<String> newFavorites) async {
    await FireBaseService.instance.updateFavorites(fireBaseUUID, [...newFavorites]);
    this.favorites = [...newFavorites];
  }

  // Delete a credit card of the user
  void deletePaymentMethod(String deletedPaymentMethoId) {
    paymentMethods.removeWhere(
            (PaymentMethod paymentMethod) => paymentMethod.paymentMethodId == deletedPaymentMethoId
    );
  }
}

class Address {
  String city;
  String line_1;
  String line_2;
  String postalCode;
  String state;

  Address(this.city, this.line_1, this.line_2, this.postalCode, this.state);

  getAddressAsJSON() {
    return {
      'city': this.city ?? '',
      'line1': this.line_1 ?? '',
      'line2': this.line_2 ?? '',
      'postal_code': this.postalCode ?? '',
      'country': 'US'
    };
  }

  getAddressAsString() {
    return jsonEncode(this.getAddressAsJSON());
  }

  factory Address.getAddressFromSnapshot(Map<dynamic, dynamic> addressSnapshot) {
    return Address(
        addressSnapshot['city'].toString(),
        addressSnapshot['line1'].toString(),
        addressSnapshot['line2'].toString(),
        addressSnapshot['postal_code'].toString(),
        addressSnapshot['state'].toString()
    );
  }
}