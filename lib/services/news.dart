// Cryptopanic news API service

import 'dart:convert';
import 'dart:core';
import 'dart:async';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:crytomarket/models/crypto_wallet.dart';

class CryptoPanic {
  final String secretKey = '375bc223277c77d53e65d3c6bd3ffffcc8be5cff';

  static CryptoPanic _instance;

  CryptoPanic();

  static void init() {
    _instance = new CryptoPanic();
  }

  static CryptoPanic get instance {
    if (_instance == null) {
      throw Exception('Attempted to get singleton instance of CryptoPanic without initialization');
    }
    return _instance;
  }

  Future<Map<String, dynamic>> fetchNewsArticles(List<String> listOfCryptoCurrencies) async {

    if(listOfCryptoCurrencies.length == 0) {
      listOfCryptoCurrencies = CryptoWallet.getSupportedCryptos();
    }

    var file = await DefaultCacheManager()
        .getSingleFile(
        'https://cryptopanic.com/api/v1/posts/?auth_token=$secretKey&currencies=${listOfCryptoCurrencies.join(",")}&kind=news&regions=en'
    );
    String fileContent = await file.readAsString();
    return json.decode(fileContent);
  }
}