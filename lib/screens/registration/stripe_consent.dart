import 'dart:core';

import 'package:flutter/material.dart';
import 'package:crytomarket/services/stripe.dart';

import 'package:crytomarket/screens/dialogs/dialog.dart';
import 'package:crytomarket/screens/registration/add_credit_card.dart';
import 'package:crytomarket/screens/utils/utils.dart';

class StripeConsent extends StatefulWidget {
  // From page to handle navigation different in registration flow.
  // There should a more graceful way to handle in navigation package. Need to explore..
  final String fromPage;

  StripeConsent({
    this.fromPage = ''
  });

  @override
  _StripeConsentState createState() => _StripeConsentState();
}

class _StripeConsentState extends State<StripeConsent> {

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {

    var goToNextPage = (bool hasDeclined) {
      if(widget.fromPage == "Registration") {
        // If in registration flow, and user has declined stripe consent take to dashboard
        if(hasDeclined) {
          Navigator.pushReplacementNamed(context, '/dashboard');
        }
        else {
          // If in registration flow, and user has accepted take to credit card form screen
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => AddCreditCardScreen(fromPage: 'Registration')),
          );
        }
      }
      else {
        Navigator.pop(context);
      }
    };

    return Scaffold(
        appBar: getAppBar('Your Stripe consent'),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Do you want to create a Stripe profile?',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'What is Stripe?',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                    'Stripe is a technology that at it\'s core is a powerful payments engine that makes moving money secure and easy.',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: 15
                    ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'What if I accept?',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'You will get a free Stripe account.\n\nStripe secures your credit card transactions to buy crypto currency.\n\nYour Stripe account gets credited when you sell cryptos for cash.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 15
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'What if I decline?',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'You can still use the app to monitor trends in crypto currency but cannot buy/sell crypto currency.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 15
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    getRaisedButton(
                      'Accept',
                      () async {
                        Stripe stripe = Stripe.instance;
                        try {
                          Dialogs.showLoadingDialog(context, _keyLoader);
                          await stripe.createNewCustomerStripeProfile();
                          Navigator.of(_keyLoader.currentContext,rootNavigator: true).pop();
                          goToNextPage(false);
                        }
                        catch(e) {
                          await Dialogs.getAlertBox(context, 'Failed', e.toString() ?? 'Empty error message');
                          goToNextPage(false);
                        }
                      }
                    ),
                    getRaisedButton(
                      'Decline',
                      () {
                        goToNextPage(true);
                      }
                    )
                  ],
                ),
              ],
            ),
          ),
        )
    );
  }
}