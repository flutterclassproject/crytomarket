import 'package:flutter/cupertino.dart';

import 'package:crytomarket/models/exchange_rates.dart';
import 'package:crytomarket/models/crypto_wallet.dart';

class CryptoExchangeDataModel extends ChangeNotifier {
  List<CryptoExchangeRate> cryptoExchangeRates = [];

  CryptoExchangeDataModel(List<dynamic> initCryptoExchangeData) {
    initCryptoExchangeData
        .where((exchangeData) =>
          CryptoWallet.getSupportedCryptos()
              .contains(exchangeData["currency"]))
        .forEach((exchangeData) =>
          cryptoExchangeRates
              .add(CryptoExchangeRate.fromJson(exchangeData)));
  }

  List<CryptoExchangeRate> getExchangeRates() {
    return cryptoExchangeRates;
  }
}