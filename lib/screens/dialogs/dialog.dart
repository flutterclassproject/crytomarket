import 'package:flutter/material.dart';

enum ConfirmationStatus {
  CONFIRMED,
  DENIED
}

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key, [String loadingText = "Please Wait...."]) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10,),
                        Text(loadingText,style: TextStyle(color: Color.fromRGBO(22, 82, 250, 1)),)
                      ]),
                    )
                  ]));
        });
  }

  static Future<void> getAlertBox(BuildContext context, String title, String msg, [String buttonText]) {
    buttonText = buttonText ?? "Ok";
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(msg),
            actions: [
              FlatButton(
                child: Text(
                  buttonText,
                  style: TextStyle(
                    color: Color.fromRGBO(22, 82, 250, 1)
                  ),
                ),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        }
    );
  }

  static Future<ConfirmationStatus> getConfirmationAlertBox(BuildContext context, String title, String msg) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(msg),
            actions: [
              FlatButton(
                child: Text(
                  "Yes",
                  style: TextStyle(
                      color: Color.fromRGBO(22, 82, 250, 1)
                  )
                ),
                onPressed: () => Navigator.of(context).pop(ConfirmationStatus.CONFIRMED)
              ),
              FlatButton(
                child: Text(
                  "Cancel",
                  style: TextStyle(
                      color: Color.fromRGBO(22, 82, 250, 1)
                  )
                ),
                onPressed: () => Navigator.of(context).pop(ConfirmationStatus.DENIED),
              ),
            ],
          );
        }
    );
  }
}