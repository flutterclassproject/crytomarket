import 'package:crytomarket/screens/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:numeric_keyboard/numeric_keyboard.dart';

import 'package:crytomarket/models/payment_method.dart';
import 'package:crytomarket/models/user.dart';
import 'package:crytomarket/screens/dashboard/confirm_buy_page.dart';
import 'package:crytomarket/screens/dialogs/dialog.dart';

import 'package:crytomarket/screens/registration/add_credit_card.dart';

class CryptoBuyScreen extends StatefulWidget {

  final String cryptoCurrencyName;

  CryptoBuyScreen({Key key, @required this.cryptoCurrencyName}) : super(key: key);

  @override
  _CryptoBuyScreenState createState() => _CryptoBuyScreenState();
}

class _CryptoBuyScreenState extends State<CryptoBuyScreen> {
  String purchaseAmount = '';
  PaymentMethod choosenPaymentMethod;

  _onKeyboardTap(String value) {
    setState(() {
      purchaseAmount = purchaseAmount + value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getAppBar('Buy now'),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  '\$ $purchaseAmount',
                  style: TextStyle(
                    color: Colors.black87,
                    fontSize: 50
                  ),
                ),
                CryptoUser.instance.paymentMethods.length > 0 ?
                  DropdownButton<PaymentMethod>(
                    hint: Text('Select a payment method'),
                    value: choosenPaymentMethod,
                    icon: Icon(Icons.arrow_drop_down),
                    iconSize: 40,
                    elevation: 16,
                    underline: Container(
                      height: 2,
                      color: Colors.black87,
                    ),
                    onChanged: (PaymentMethod newValue) {
                      setState(() {
                        choosenPaymentMethod = newValue;
                      });
                    },
                    items: CryptoUser.instance.paymentMethods
                        .map<DropdownMenuItem<PaymentMethod>>((PaymentMethod paymentMethod) {
                      CustomerCreditCard card = paymentMethod.card;
                      String brand = card.brand;
                      return DropdownMenuItem<PaymentMethod>(
                        value: paymentMethod,
                        child: Text(
                          '${brand[0].toUpperCase()}${brand.substring(1)} ending with ${card.lastFourDigits}',
                          style: TextStyle(
                            fontSize: 20
                          ),
                        )
                      );
                    }).toList(),
                  ):
                  RaisedButton(
                    child: new Text('Add a payment method'),
                    onPressed: () {
                      Navigator
                          .push(context, MaterialPageRoute(builder: (context) => AddCreditCardScreen()))
                          .then((value) => setState(() {
                            choosenPaymentMethod = null;
                            purchaseAmount = '';
                          }));
                    }
                  ),
                NumericKeyboard(
                  onKeyboardTap: _onKeyboardTap,
                  textColor: Colors.black87,
                  rightButtonFn: () {
                    if(purchaseAmount.length > 0) {
                      setState(() {
                        purchaseAmount = purchaseAmount.substring(0, purchaseAmount.length - 1);
                      });
                    }
                  },
                  rightIcon: Icon(
                    Icons.backspace,
                    color: Colors.black87,
                  ),
                  leftButtonFn: () {
                    setState(() {
                      if(purchaseAmount.indexOf('.') == -1) {
                        purchaseAmount = purchaseAmount+'.';
                      }
                    });
                  },
                  leftIcon: Icon(
                    FontAwesomeIcons.solidCircle,
                    size: 10,
                    color: Colors.black87,
                  ),
                ),
                getRaisedButton(
                  'Preview Buy',
                  () {
                    if(purchaseAmount.length == 0) {
                      Dialogs.getAlertBox(context, 'Warning', 'Please enter an amount');
                      return;
                    }
                    if(choosenPaymentMethod == null) {
                      Dialogs.getAlertBox(context, 'Warning', 'Please choose a payment method');
                      return;
                    }
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) => PreviewBuyScreen(
                        cryptoCurrencyName: widget.cryptoCurrencyName,
                        paymentAmount: purchaseAmount,
                        paymentMethod: choosenPaymentMethod,
                      ),
                    ));
                  }
                ),
              ],
            ),
          ),
        ),
    );
  }
}