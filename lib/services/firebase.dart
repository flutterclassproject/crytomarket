// Firebase service

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FireBaseService {
  static FireBaseService _instance;
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final DatabaseReference dbRef = FirebaseDatabase.instance.reference().child("Users");

  FireBaseService();

  static void init() {
    _instance = FireBaseService();
  }

  static FireBaseService get instance {
    if (_instance == null) {
      throw Exception('Attempted to get singleton instance of Firebase service without initialization');
    }
    return _instance;
  }

  Future<UserCredential> loginWithEmailAndPassword(String email, String password) async {
    return await firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<DataSnapshot> getUserProfile(String firebaseUUID) async {
    return await dbRef.child(firebaseUUID).once();
  }

  Future<void> updateUserWallet(String fireBaseUUID, Map<String, String> cryptoWallet) async {
    return await dbRef.child(fireBaseUUID).update({
      'wallet': cryptoWallet
    });
  }

  Future<void> updateFavorites(String fireBaseUUID, List<String> favorites) async {
    return await dbRef.child(fireBaseUUID).update({
      'favorites': favorites
    });
  }

  Future<String> createUserProfile(String email, String password, String phone, String name) async {
    UserCredential firebaseUserProfile = await firebaseAuth.createUserWithEmailAndPassword(
      email: email, password: password
    );
    await dbRef.child(firebaseUserProfile.user.uid).set({
      "email": email,
      "phone": phone,
      "name": name,
      'stripe_consent': false,
      "wallet" : {}
    });
    return firebaseUserProfile.user.uid;
  }

  Future<void> sendPasswordResetEmail(String email) async {
    return await firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<void> signout() async {
    await firebaseAuth.signOut();
  }
}