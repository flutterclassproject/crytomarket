// Provider of user's favorites crypto currencies.

import 'package:flutter/cupertino.dart';
import 'package:crytomarket/models/user.dart';

class CryptoFavoritesModel extends ChangeNotifier {
  List<String> favoriteCryptoCurrencies = [];

  CryptoFavoritesModel(List<String> initFavorites) {
    initFavorites
        .forEach((String favorite) => favoriteCryptoCurrencies.add(favorite));
  }

  List<String> getFavorites() {
    return favoriteCryptoCurrencies;
  }

  List<String> getFavoriteCryptos() {
    return favoriteCryptoCurrencies;
  }

  void addToFavorites(String cryptoId) {
    // Since notifyListeners() is called after firebase database call to add crypto to favorites list, the UI update will not be immediate.
    favoriteCryptoCurrencies.add(cryptoId);
    CryptoUser.instance
        .updateFavorites(favoriteCryptoCurrencies)
        .then((value) => this.notifyListeners())
        .catchError((error) => print("Failed to add favorite $cryptoId"));
  }

  void removeFromFavorites(String cryptoId) {
    favoriteCryptoCurrencies.remove(cryptoId);
    CryptoUser.instance.updateFavorites(favoriteCryptoCurrencies);
    CryptoUser.instance
        .updateFavorites(favoriteCryptoCurrencies)
        .then((value) => this.notifyListeners())
        .catchError((error) => print("Failed to delete favorite $cryptoId"));
  }
}