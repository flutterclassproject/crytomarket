// Nomics API service

import 'dart:convert';
import 'dart:core';
import 'dart:async';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:crytomarket/models/crypto_wallet.dart';

class Nomics {

  final String secretKey = 'aa9fa52687fc82fdf8c81a85a379f72b';

  static Nomics _instance;

  Nomics();

  static void init() {
    _instance = new Nomics();
  }

  static Nomics get instance {
    if (_instance == null) {
      throw Exception('Attempted to get singleton instance of Nomics without initialization');
    }
    return _instance;
  }

  // Get crypto currencies ticker data
  Future<List<dynamic>> getCurrenciesTickData([List<String> listOfCryptoCurrencies, List<String> intervals]) async {
    listOfCryptoCurrencies = listOfCryptoCurrencies ?? CryptoWallet.getSupportedCryptos();
    intervals = intervals ?? CryptoWallet.getSupportedIntervals();
    var file = await DefaultCacheManager()
        .getSingleFile(
        'https://api.nomics.com/v1/currencies/ticker?key=$secretKey&ids=${listOfCryptoCurrencies.join(",")}&interval=${intervals.join(",")}'
    );
    String fileContent = await file.readAsString();
    return json.decode(fileContent);
  }

  // This API not used. Ignore
  Future<List<dynamic>> getExchangeRates() async {
    var file = await DefaultCacheManager()
        .getSingleFile(
        'https://api.nomics.com/v1/exchange-rates?key=$secretKey'
    );
    String fileContent = await file.readAsString();
    return json.decode(fileContent);
  }

  // Get crypto currencies metadata
  Future<List<dynamic>> getCryptoMetaData([List<String> listOfCryptoCurrencies]) async {
    listOfCryptoCurrencies = listOfCryptoCurrencies ?? CryptoWallet.getSupportedCryptos();
    var file = await DefaultCacheManager()
        .getSingleFile(
        'https://api.nomics.com/v1/currencies?key=$secretKey&ids=${listOfCryptoCurrencies.join(",")}'
    );
    String fileContent = await file.readAsString();
    return json.decode(fileContent);
  }

  // Get crypto currencies sparkline data (price history)
  Future<List<dynamic>> getCryptoSparkLineData([List<String> listOfCryptoCurrencies, String startDate, String endDate]) async {
    listOfCryptoCurrencies = listOfCryptoCurrencies ?? CryptoWallet.getSupportedCryptos();
    var file = await DefaultCacheManager()
        .getSingleFile(
        'https://api.nomics.com/v1/currencies/sparkline?key=$secretKey&ids=${listOfCryptoCurrencies.join(",")}&start=$startDate&end=$endDate'
    );
    String fileContent = await file.readAsString();
    return json.decode(fileContent);
  }
}