// Stripe service for payment and credits

import 'dart:core';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

import 'package:crytomarket/models/user.dart';
import 'package:crytomarket/models/crypto_wallet.dart';
import 'package:firebase_database/firebase_database.dart';

class Stripe {

  final String secretKey = 'sk_test_51HrfsmJHL2xlopBpjUn54F2IQuYIIgnjNSXbd0smOdEv03baH2TxkY7zvwx1xdMZs4bwq1YkMrW5NU1xEoHtXiry004rA1iF5y';

  static Stripe _instance;

  Map<String, dynamic> stripeProfile = {};

  Stripe();

  static void init() {
    _instance = new Stripe();
  }

  static Stripe get instance {
    if (_instance == null) {
      throw Exception('Attempted to get singleton instance of Stripe without initialization');
    }
    return _instance;
  }


  static String _urlEncodeMap(dynamic data) {
    final urlData = StringBuffer('');
    var first = true;
    void urlEncode(dynamic sub, String path) {
      if (sub is List) {
        for (var i = 0; i < sub.length; i++) {
          urlEncode(sub[i], '$path%5B%5D');
        }
      } else if (sub is Map) {
        sub.forEach((k, v) {
          if (path == '') {
            urlEncode(v, '${Uri.encodeQueryComponent(k)}');
          } else {
            urlEncode(v, '$path%5B${Uri.encodeQueryComponent(k)}%5D');
          }
        });
      } else {
        if (!first) {
          urlData.write('&');
        }
        first = false;
        urlData.write('$path=${Uri.encodeQueryComponent(sub.toString())}');
      }
    }

    urlEncode(data, '');
    return urlData.toString();
  }

  Future<void> refreshCustomerStripeProfile() async {
    String stripeCustomerId = 'cus_ISool2U6T7u9hP';
    Response response = await get(
        'https://api.stripe.com/v1/customers/$stripeCustomerId',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    stripeProfile = responseData;
  }

  // Create a new Stripe customer profile.
  Future<void> createNewCustomerStripeProfile() async {
    CryptoUser user = CryptoUser.instance;
    final Map<String, dynamic> postData = user.getContactDetails();
    Response response = await post(
        'https://api.stripe.com/v1/customers',
        body: Stripe._urlEncodeMap(postData),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 && responseData.containsKey('id')) {
      stripeProfile = responseData;
      DatabaseReference dbRef = FirebaseDatabase.instance.reference().child("Users");
      String stripeCustomerId = responseData['id'].toString();
      await dbRef.child(user.fireBaseUUID).update({
        'stripe_customer_id': stripeCustomerId,
        'stripe_consent': true,
        'wallet': CryptoWallet.getEmptyCryptoWallet()
      });
      user.stripeCustomerId = stripeCustomerId;
      user.stripeConsent = true;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Adding credit card has two steps - Create a payment method and attaching it to a customer

  // Attach a credit card to a customer
  Future<void> attachCreditCardToCustomer(String paymentMethodId) async {

    CryptoUser user = CryptoUser.instance;

    final Map<String, dynamic> postData = {
      'customer': user.stripeCustomerId
    };

    Response response = await post(
        'https://api.stripe.com/v1/payment_methods/$paymentMethodId/attach',
        body: Stripe._urlEncodeMap(postData),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> cardResponseData = json.decode(response.body);
    if(response.statusCode == 200 && cardResponseData.containsKey('id')) {
      user.addPaymentMethod(cardResponseData);
    }
    else {
      throw new Exception(cardResponseData['error']['message'].toString());
    }
  }

  // Creating a payment method
  Future<void> addCreditCard(String cardNumber, String expMonth, String expYear, String cvv) async {
    CryptoUser user = CryptoUser.instance;

    final Map<String, dynamic> postData = {
      'billing_details': user.getContactDetails(),
      'type': 'card',
      'card': {
        'number': cardNumber,
        'exp_month': expMonth,
        'exp_year': expYear,
        'cvc': cvv
      }
    };

    Response response = await post(
        'https://api.stripe.com/v1/payment_methods',
        body: Stripe._urlEncodeMap(postData),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 && responseData.containsKey('id')) {
      String paymentMethodId = responseData['id'];
      await this.attachCreditCardToCustomer(paymentMethodId);
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Fetch a customers payment methods(credit cards)
  Future<Map<String, dynamic>> loadPaymentMethods() async {

    CryptoUser user = CryptoUser.instance;
    String stripeCustomerId = user.getStripeCustomerId();

    Response response = await get(
        'https://api.stripe.com/v1/payment_methods?customer=$stripeCustomerId&type=card',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }


  // Payment via credit card has two steps - Create a payment intent and confirm it by attaching a payment method to it

  // Create a payment intent
  Future<Map<String, dynamic>> createPaymentIntent(int amount, String paymentMethodId, [Map<String, String> metaData]) async {

    CryptoUser user = CryptoUser.instance;
    String stripeCustomerId = user.getStripeCustomerId();

    final Map<String, dynamic> postData = {
      'customer': stripeCustomerId,
      'amount': amount,
      'currency': 'usd',
      'metadata': metaData ?? {}
    };

    Response response = await post(
        'https://api.stripe.com/v1/payment_intents',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        },
        body: Stripe._urlEncodeMap(postData),
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 &&
        responseData.containsKey('id')) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Confirm payment
  Future<Map<String, dynamic>> confirmPayment(String paymentIntentId, String paymentMethodId) async {

    final Map<String, dynamic> postData = {
      'payment_method': paymentMethodId,
    };

    Response response = await post(
        'https://api.stripe.com/v1/payment_intents/$paymentIntentId/confirm',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        },
        body: Stripe._urlEncodeMap(postData),
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 &&
        responseData.containsKey('id') &&
        responseData.containsKey('object') &&
        responseData['object'] == 'payment_intent') {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Crediting a customer stripe account has two steps - Create a credit transaction and confirm it.

  // Create a transaction
  Future<Map<String, dynamic>> createCreditAmountTransaction(int amount, [Map<String, String> metaData]) async {

    CryptoUser user = CryptoUser.instance;
    String stripeCustomerId = user.getStripeCustomerId();

    final Map<String, dynamic> postData = {
      'amount': amount,
      'currency': 'usd',
      'metadata': metaData ?? {}
    };

    Response response = await post(
      'https://api.stripe.com/v1/customers/$stripeCustomerId/balance_transactions',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
      },
      body: Stripe._urlEncodeMap(postData),
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 &&
      responseData.containsKey('id') &&
      responseData.containsKey('object') &&
      responseData['object'].toString() == "customer_balance_transaction"
    ) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Confirm a transaction
  Future<Map<String, dynamic>> confirmCreditTransaction(String creditTransactionId) async {

    CryptoUser user = CryptoUser.instance;
    String stripeCustomerId = user.getStripeCustomerId();

    Response response = await post(
      'https://api.stripe.com/v1/customers/$stripeCustomerId/balance_transactions/$creditTransactionId',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
      },
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 &&
        responseData.containsKey('id') &&
        responseData.containsKey('object') &&
        responseData['object'].toString() == "customer_balance_transaction"
    ) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Delete a payment method attached to a customer
  Future<Map<String, dynamic>> deletePaymentMethod(String paymentMethodId) async {
    Response response = await post(
      'https://api.stripe.com/v1/payment_methods/$paymentMethodId/detach',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
      },
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200 &&
        responseData.containsKey('id')
    ) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Fetch all crypto purchase transactions
  Future<Map<String, dynamic>> fetchCryptoBuyTransactions([int noOfLastTransactions = 10]) async {
    CryptoUser user = CryptoUser.instance;
    String stripeCustomerId = user.getStripeCustomerId();

    Response response = await get(
        'https://api.stripe.com/v1/payment_intents?customer=$stripeCustomerId&limit=$noOfLastTransactions',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }

  // Fetch all crypto sales transactions
  Future<Map<String, dynamic>> fetchCryptoSellTransactions([int noOfLastTransactions = 10]) async {
    CryptoUser user = CryptoUser.instance;
    String stripeCustomerId = user.getStripeCustomerId();

    Response response = await get(
        'https://api.stripe.com/v1/customers/$stripeCustomerId/balance_transactions?limit=$noOfLastTransactions',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic ' + base64.encode(utf8.encode(secretKey)),
        }
    );
    final Map<String, dynamic> responseData = json.decode(response.body);
    if(response.statusCode == 200) {
      return responseData;
    }
    else {
      throw new Exception(responseData['error']['message'].toString());
    }
  }
}