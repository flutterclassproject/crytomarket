// Provider of Crypto currencies metadata model

import 'package:flutter/cupertino.dart';

import 'package:crytomarket/models/crypto_metadata.dart';

class CryptoMetaDataModel extends ChangeNotifier {
  List<CryptoMetaData> cryptoMetaData = [];

  CryptoMetaDataModel(List<dynamic> initCryptoMetaData) {
    initCryptoMetaData
        .forEach((metaData) =>
        cryptoMetaData
            .add(CryptoMetaData.fromJson(metaData)));
  }

  List<CryptoMetaData> getCryptoMetaData() {
    return cryptoMetaData;
  }
}