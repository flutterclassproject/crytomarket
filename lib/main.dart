import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:crytomarket/screens/login/login.dart';
import 'package:crytomarket/screens/dashboard/dashboard.dart';

import 'package:crytomarket/services/stripe.dart';
import 'package:crytomarket/services/nomics.dart';
import 'package:crytomarket/services/firebase.dart';
import 'package:crytomarket/services/news.dart';

import 'package:crytomarket/models/user.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Stripe.init();
  CryptoUser.init();
  Nomics.init();
  FireBaseService.init();
  CryptoPanic.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: '/',
      routes: {
        '/': (context) => Login(),
        '/dashboard': (context) => Dashboard()
      },
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}
