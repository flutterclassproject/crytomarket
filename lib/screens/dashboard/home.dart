import 'package:crytomarket/models/crypto_ticker_data.dart';
import 'package:crytomarket/models/crypto_wallet.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:crytomarket/providers/crypto_favorites_provider.dart';
import 'package:crytomarket/providers/crypto_ticker_provider.dart';

import 'package:crytomarket/services/nomics.dart';
import 'package:crytomarket/services/news.dart';

import 'package:crytomarket/screens/utils/utils.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:timeago/timeago.dart' as timeago;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {

    Future<List<dynamic>> _fetchCurrenciesTickerData = Nomics.instance.getCurrenciesTickData();

    return FutureBuilder<List<dynamic>>(
      future: _fetchCurrenciesTickerData,
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        Widget pricesListWidget;
        if(snapshot.hasData) {
          pricesListWidget =  ChangeNotifierProvider(
              create: (context) => CryptoTickerDataModel(snapshot.data),
              child: HomeScreen()
          );
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          pricesListWidget =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to load crypto prices'
          );
        }
        else {
          pricesListWidget =  getScreenCircularLoader();
        }
        return Scaffold(
          appBar: getAppBar('Home'),
          body: Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: pricesListWidget,
              ),
            ),
          ),
        );
      },
    );
  }
}

class FavoriteCryptos extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final cryptoFavoritesProvider = Provider.of<CryptoFavoritesModel>(context);
    List<String> favorites = cryptoFavoritesProvider.getFavoriteCryptos();

    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context);

    var buildRow = (String cryptoId) {
      CryptoTickerData tickerData = cryptoTickerDataProvider.getCryptoTickerData(cryptoId);
      String oneHourPriceChangePercent = tickerData.oneHourIntervalData.priceChangePercent;
      String cryptoPrice = tickerData.priceData.price;

      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            getLeadingWidget(cryptoId),
            Column(
              children: [
                Text(
                  tickerData.name,
                  style: TextStyle(
                    fontSize: 15,
                  )
                ),
                Text(
                  cryptoId,
                  style: TextStyle(
                    fontSize: 15,
                  )
                ),
              ],
            ),
            Column(
              children: [
                Text(
                    adjustPrice(cryptoPrice),
                    style: TextStyle(
                      fontSize: 15,
                    )
                ),
                Row(
                    children: <Widget>[
                      getArrow(oneHourPriceChangePercent),
                      Text(
                        '$oneHourPriceChangePercent%',
                        style: TextStyle(
                            fontSize: 15,
                            color: getColorForPriceChangePercent(oneHourPriceChangePercent)
                        ),
                      )
                    ]
                )
              ],
            )
          ],
        ),
      );
    };

    var emptyFavoritesWidget = () {
      return Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Click on '),
            Icon(
              Icons.favorite_border,
              color: Colors.red,
            ),
            Text(' to add a favorite')
          ],
        ),
      );
    };

    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Favorites",
            style: TextStyle(
              fontSize: 25
            ),
          ),
          Card(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: favorites.length > 0 ? Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: favorites.map((String favorite) => buildRow(favorite)).toList(),
              ): emptyFavoritesWidget(),
            ),
          )
        ],
      ),
    );
  }
}

class TopMovers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context);

    var buildTopMoverCard = (String cryptoId) {
      CryptoTickerData tickerData = cryptoTickerDataProvider.getCryptoTickerData(cryptoId);
      String cryptoPrice = tickerData.priceData.price;
      String oneHourPriceChangePercent = tickerData.oneHourIntervalData.priceChangePercent;

      return Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                children: [
                  getLeadingWidget(cryptoId)
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text(
                      tickerData.name
                    ),
                    SizedBox(width: 5,),
                    Text(
                      adjustPrice(cryptoPrice)
                    )
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  getArrow(oneHourPriceChangePercent),
                  Text(
                    '$oneHourPriceChangePercent%',
                    style: TextStyle(
                        fontSize: 25,
                        color: getColorForPriceChangePercent(oneHourPriceChangePercent)
                    ),
                  )
                ]
              )
            ],
          ),
        ),
      );
    };

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Top Movers",
            style: TextStyle(
                fontSize: 25
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height/5,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: cryptoTickerDataProvider.getTopMovers().map((CryptoTickerData tickerData) => buildTopMoverCard(tickerData.id)).toList()
            ),
          ),
        ],
      ),
    );
  }
}


class NewsFeedRow extends StatelessWidget {
  final String newsSource;
  final String publishedDate;
  final String title;
  final String urlLink;
  final String aboutWhichCrypto;

  NewsFeedRow({
    Key key,
    this.newsSource = '',
    this.publishedDate = '',
    this.title = '',
    this.urlLink = '',
    this.aboutWhichCrypto = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        if (await canLaunch(urlLink)) {
          await launch(urlLink);
        } else {
        throw 'Could not launch $urlLink';
        }
      },
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 5.0),
                width: MediaQuery.of(context).size.width*0.8,
                child: RichText(
                  textAlign: TextAlign.justify,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  text: TextSpan(
                    style: TextStyle(
                      color: Colors.black
                    ),
                    children: [
                      TextSpan(text: '$newsSource  '),
                      TextSpan(text: '  ${timeago.format(DateTime.parse(publishedDate))}   '),
                      TextSpan(
                        text: '  $aboutWhichCrypto',
                        style: TextStyle(
                          color: Colors.orangeAccent
                        )
                      ),
                    ],
                  ),
                )
              ),
            ],
          ),
          Row(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 5.0),
                width: MediaQuery.of(context).size.width*0.8,
                child: Text(
                  title,
                  textAlign: TextAlign.justify,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class NewsFeedBuilder extends StatelessWidget {

  final Map<String, dynamic> newsFeedJson;
  final int numberOfArticles;

  NewsFeedBuilder({
    Key key,
    @required this.newsFeedJson,
    this.numberOfArticles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<dynamic> newsList = newsFeedJson.containsKey('results') ? newsFeedJson['results'] : [];
    return ListView.separated(
      primary: false,
      shrinkWrap: true,
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemCount: numberOfArticles ?? newsList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: NewsFeedRow(
            newsSource: newsList[index]['source']['title'] ?? '',
            publishedDate: newsList[index]['published_at'] ?? '',
            title: newsList[index]['title'] ?? '',
            urlLink: newsList[index]['url'] ?? '',
            aboutWhichCrypto: newsList[index]['currencies'][0]['title'] ?? '',
          ),
        );
      },
    );
  }
}

class NewsArticlesList extends StatelessWidget {

  final Future<Map<String, dynamic>> newsFuture;
  final int numberOfArticles;

  NewsArticlesList({
    Key key,
    @required this.newsFuture,
    this.numberOfArticles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return FutureBuilder<Map<String, dynamic>>(
      future: newsFuture,
      builder: (BuildContext context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
        Widget pricesListWidget;
        if(snapshot.hasData) {
          pricesListWidget = NewsFeedBuilder(
            newsFeedJson: snapshot.data,
            numberOfArticles: numberOfArticles,
          );
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          pricesListWidget =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to fetch news'
          );
        }
        else {
          pricesListWidget =  getScreenCircularLoader();
        }
        return pricesListWidget;
      },
    );
  }
}

class FavoriteCryptoNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final cryptoFavoritesProvider = Provider.of<CryptoFavoritesModel>(context);
    List<String> favorites = cryptoFavoritesProvider.getFavoriteCryptos();

    Future<Map<String, dynamic>> fetchNewsArticles = CryptoPanic.instance.fetchNewsArticles(favorites);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "News",
                style: TextStyle(
                  fontSize: 25,
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) => MoreNews(),
                  ));
                },
                child: Text(
                  "View more",
                  style: TextStyle(
                    fontSize: 25,
                    color: new Color.fromRGBO(22, 82, 250, 1)
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.symmetric(vertical: 2.0),
            width: MediaQuery.of(context).size.width,
            child: NewsArticlesList(
              newsFuture: fetchNewsArticles,
              numberOfArticles: 5,
            )
          ),
        ],
      ),
    );
  }
}

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height/4,
      width: MediaQuery.of(context).size.width,
      child: Image.asset(
        'logo.png',
        fit: BoxFit.contain,
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Logo(),
        FavoriteCryptos(),
        TopMovers(),
        FavoriteCryptoNews()
      ],
    );
  }
}


class MoreNews extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Future<Map<String, dynamic>> fetchAllCryptoNewsArticles = CryptoPanic.instance
        .fetchNewsArticles(CryptoWallet.getSupportedCryptos());

    return Scaffold(
      appBar: getAppBar('News'),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: NewsArticlesList(
            newsFuture: fetchAllCryptoNewsArticles
          ),
        ),
      ),
    );
  }
}