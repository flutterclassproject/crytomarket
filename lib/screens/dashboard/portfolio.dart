import 'package:crytomarket/models/crypto_metadata.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:crytomarket/services/nomics.dart';
import 'package:crytomarket/providers/crypto_ticker_provider.dart';
import 'package:crytomarket/providers/crypto_metadata_provider.dart';
import 'package:crytomarket/models/crypto_ticker_data.dart';

import 'package:crytomarket/screens/utils/utils.dart';

class Portfolio extends StatefulWidget {
  @override
  _PortfolioState createState() => _PortfolioState();
}

class _PortfolioState extends State<Portfolio> {

  @override
  Widget build(BuildContext context) {
    Future<List<dynamic>> _fetchCurrenciesExchangeData = Nomics.instance.getCurrenciesTickData();
    Future<List<dynamic>> _fetchCurrenciesMetaData = Nomics.instance.getCryptoMetaData();
    return FutureBuilder<List<dynamic>>(
      future: Future.wait([
        _fetchCurrenciesExchangeData,
        _fetchCurrenciesMetaData,
      ]),
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        Widget pricesListWidget;
        if(snapshot.hasData) {
          pricesListWidget =  MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (context) => CryptoTickerDataModel(snapshot.data[0])),
              ChangeNotifierProvider(create: (context) => CryptoMetaDataModel(snapshot.data[1])),
            ],
            child: CryptoListView(),
          );
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          pricesListWidget =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to load crypto prices'
          );
        }
        else {
          pricesListWidget =  SizedBox(
            child: CircularProgressIndicator(),
            width: 30,
            height: 30,
          );
        }
        return Scaffold(
          appBar: getAppBar('Your wallet'),
          body: Center(
            child: pricesListWidget,
          ),
        );
      }
    );
  }
}

class CryptoCoinListView extends StatelessWidget {
  CryptoCoinListView(this.crypoList, this.cryptoMetaData);
  final List<CryptoTickerData> crypoList;
  final List<CryptoMetaData> cryptoMetaData;

  Widget buildRow(BuildContext context, CryptoTickerData cryptoInfo, CryptoMetaData cryptoMetaData) {
    return ListTile(
        leading: getLeadingWidget(cryptoInfo.currency),
        title: Text(cryptoMetaData.name),
        trailing: showTrailngWalletBalance(cryptoInfo.id, cryptoInfo.priceData.price),
        subtitle: Text(cryptoInfo.currency),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: crypoList.length,
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          final index = i;
          CryptoMetaData metaData = cryptoMetaData
              .firstWhere((CryptoMetaData metaData) => metaData.id == crypoList[index].id);
          return buildRow(context, crypoList[index], metaData); //build the row widget
        });
  }
}

class CryptoListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    final cryptoMetaDataProvider = Provider.of<CryptoMetaDataModel>(context, listen: false);
    return CryptoCoinListView(cryptoTickerDataProvider.getAllCryptos(), cryptoMetaDataProvider.getCryptoMetaData());
  }
}