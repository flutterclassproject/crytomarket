import 'package:flutter/cupertino.dart';

import 'package:crytomarket/models/crypto_ticker_data.dart';

class CryptoTickerDataModel extends ChangeNotifier {
  List<CryptoTickerData> cryptoTickerData = [];
  static int noOfTopMovers = 5;

  CryptoTickerDataModel(List<dynamic> initCryptoTickerData) {
    initCryptoTickerData.forEach((currencyTickerData) {
      cryptoTickerData.add(CryptoTickerData.fromJson(currencyTickerData));
    });
  }

  // Get cryptos with positive price change in last one hour
  List<CryptoTickerData> getGainers() {
    return cryptoTickerData.where(
            (CryptoTickerData cryptoInfo) => !(double.parse(cryptoInfo.oneHourIntervalData.priceChangePercent).isNegative)
    ).toList();
  }

  // Get cryptos with negative price change in last one hour
  List<CryptoTickerData> getLosers() {
    return cryptoTickerData.where(
            (CryptoTickerData cryptoInfo) => double.parse(cryptoInfo.oneHourIntervalData.priceChangePercent).isNegative
    ).toList();
  }

  List<CryptoTickerData> getAllCryptos() {
    return cryptoTickerData;
  }

  CryptoTickerData getCryptoTickerData(String cryptoId) {
    return cryptoTickerData.firstWhere((CryptoTickerData cryptoInfo) => cryptoInfo.id == cryptoId);
  }

  // Get top 5 cryptos with highest price change
  List<CryptoTickerData> getTopMovers() {
    List<CryptoTickerData> copyTickerData = [...cryptoTickerData];
    copyTickerData.sort(
            (CryptoTickerData a, CryptoTickerData b) =>
                double.parse(a.oneHourIntervalData.priceChangePercent).abs()
                    .compareTo(double.parse(b.oneHourIntervalData.priceChangePercent).abs())
    );
    return copyTickerData.reversed.toList().sublist(0, noOfTopMovers);
  }
}