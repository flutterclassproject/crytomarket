import 'package:crytomarket/services/nomics.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:crytomarket/providers/crypto_ticker_provider.dart';
import 'package:crytomarket/providers/crypto_favorites_provider.dart';
import 'package:crytomarket/models/crypto_ticker_data.dart';

import 'package:crytomarket/screens/utils/utils.dart';
import 'package:crytomarket/screens/dashboard/crypto_detailed_page.dart';


class Prices extends StatefulWidget {
  @override
  _PricesState createState() => _PricesState();
}

class _PricesState extends State<Prices> {

  @override
  Widget build(BuildContext context) {
    Future<List<dynamic>> _fetchCurrenciesTickerData = Nomics.instance.getCurrenciesTickData();
    return FutureBuilder<List<dynamic>>(
      future: _fetchCurrenciesTickerData,
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        Widget pricesListWidget;
        if(snapshot.hasData) {
          // print(snapshot.data);
          return ChangeNotifierProvider(
              create: (context) => CryptoTickerDataModel(snapshot.data),
              child: CryptoCoinsTabView()
          );
        }
        else if(snapshot.hasError) {
          print(snapshot.error.toString());
          pricesListWidget =  getErrorScreen(
              Icons.error_outline,
              Colors.red, 'Failed to load crypto prices'
          );
        }
        else {
          pricesListWidget =  getScreenCircularLoader();
        }
        return Scaffold(
            body: Center(
                child: pricesListWidget
            )
        );
      },
    );
  }
}

class CryptoCoinListView extends StatelessWidget {
  CryptoCoinListView(this.crypoList);
  final List<CryptoTickerData> crypoList;

  Widget getPriceDataWidget(String price, String priceChangePercent) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Price - ${adjustPrice(price)}',
          style: TextStyle(
            fontSize: 15
          ),
        ),
        Row(
            children: <Widget>[
              getArrow(priceChangePercent),
              Text(
                '$priceChangePercent%',
                style: TextStyle(
                    fontSize: 15,
                    color: getColorForPriceChangePercent(priceChangePercent)
                ),
              )
            ]
        )
      ],
    );
  }

  Widget buildRow(BuildContext context,  CryptoTickerData cryptoInfo, Color color, bool isFavorite, Function onFavChange) {

    return ListTile(
      leading: getLeadingWidget(cryptoInfo.id, color),
      title: Text(cryptoInfo.name),
      // trailing: new IconButton(icon: Icon(Icons.favorite, color: Colors.red,), onPressed: null),
      subtitle: getPriceDataWidget(cryptoInfo.priceData.price, cryptoInfo.oneHourIntervalData.priceChangePercent),
      trailing: new IconButton(
        icon: Icon(isFavorite
            ? Icons.favorite
            : Icons
            .favorite_border),
        color:
        isFavorite ? Colors.red : null,
        onPressed: onFavChange,
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => CryptoDetailedPage(cryptoCurrencyName: cryptoInfo.id),
        ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final cryptoFavoritesProvider = Provider.of<CryptoFavoritesModel>(context);

    return ListView.builder(
        itemCount: crypoList.length,
        padding:
        const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          final index = i;
          String cryptoId = crypoList[index].id;
          bool isFavorite = cryptoFavoritesProvider.getFavoriteCryptos().contains(cryptoId);
          Function updateFavorites = () {
            if(isFavorite) {
              cryptoFavoritesProvider.removeFromFavorites(cryptoId);
              return;
            }
            cryptoFavoritesProvider.addToFavorites(cryptoId);
          };
          return buildRow(context, crypoList[index], Colors.primaries[index], isFavorite, updateFavorites); //build the row widget
        });
  }
}

class AllCryptoCoinsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    return CryptoCoinListView(cryptoTickerDataProvider.getAllCryptos());
  }
}

class GainersCryptoCoinList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    return CryptoCoinListView(cryptoTickerDataProvider.getGainers());
  }
}

class LosersCryptoCoinList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    return CryptoCoinListView(cryptoTickerDataProvider.getLosers());
  }
}

class CryptoCoinsTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          bottom: TabBar(
            indicatorColor: new Color.fromRGBO(22, 82, 250, 1),
            labelColor: Colors.black87,
            labelStyle: TextStyle(
              fontSize: 15
            ),
            tabs: [
              Tab(text: 'All assets'),
              Tab(text: 'Gainers'),
              Tab(text: 'Losers'),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            AllCryptoCoinsList(),
            GainersCryptoCoinList(),
            LosersCryptoCoinList(),
          ],
        ),
      ),
    );
  }
}

