import 'package:flutter/material.dart';

import 'package:crytomarket/services/stripe.dart';
import 'package:crytomarket/services/nomics.dart';

import 'package:provider/provider.dart';
import 'package:crytomarket/providers/crypto_ticker_provider.dart';

import 'package:crytomarket/screens/utils/utils.dart';
import 'package:crytomarket/screens/dialogs/dialog.dart';

import 'package:crytomarket/models/crypto_ticker_data.dart';
import 'package:crytomarket/models/payment_method.dart';
import 'package:crytomarket/models/user.dart';

class PreviewBuyScreen extends StatefulWidget {

  final PaymentMethod paymentMethod;
  final String paymentAmount;
  final String cryptoCurrencyName;

  PreviewBuyScreen({
    Key key,
    @required this.paymentMethod,
    @required this.paymentAmount,
    @required this.cryptoCurrencyName
  }) : super(key: key);

  @override
  _PreviewBuyScreenState createState() => _PreviewBuyScreenState();
}

class _PreviewBuyScreenState extends State<PreviewBuyScreen> {

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {

    Future<List<dynamic>> _fetchCurrencyExchangeData = Nomics.instance.getCurrenciesTickData([widget.cryptoCurrencyName]);
    return FutureBuilder<List<dynamic>>(
        future: _fetchCurrencyExchangeData,
        builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
          Widget paymentSummaryWidget;
          if(snapshot.hasData) {
            paymentSummaryWidget =  MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (context) => CryptoTickerDataModel(snapshot.data)),
              ],
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    PurchasedCryptoCount(paymentAmount: widget.paymentAmount),
                    PurchaseSummary(
                        paymentAmount: widget.paymentAmount,
                        paymentMethod: widget.paymentMethod
                    ),
                    ConfirmPaymentButton(
                      paymentMethod: widget.paymentMethod,
                      paymentAmount: widget.paymentAmount,
                      keyLoader: _keyLoader,
                    )
                  ],
                ),
              ),
            );
          }
          else if(snapshot.hasError) {
            print(snapshot.error.toString());
            paymentSummaryWidget =  getErrorScreen(
                Icons.error_outline,
                Colors.red, 'Failed to load crypto prices - ${snapshot.error.toString() ?? ''}'
            );
          }
          else {
            paymentSummaryWidget = getScreenCircularLoader();
          }
          return Scaffold(
            appBar: getAppBar('Payment Summary'),
            body: Center(
              child: paymentSummaryWidget
            )
          );
        }
    );
  }
}

class ConfirmPaymentButton extends StatelessWidget {
  final String paymentAmount;
  final PaymentMethod paymentMethod;
  final GlobalKey<State> keyLoader;

  ConfirmPaymentButton({
    @required this.keyLoader,
    @required this.paymentAmount,
    @required this.paymentMethod
  });

  @override
  Widget build(BuildContext context) {

    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    CryptoTickerData tickerData = cryptoTickerDataProvider.getAllCryptos()[0];

    String cryptoUnitCount = getNumberCryptoUnitsForPayment(paymentAmount, tickerData.priceData.price);

    var confirmPayment = (String paymentIntentId, String paymentMethodId) async {
      Stripe stripe = Stripe.instance;
      try {
        Dialogs.showLoadingDialog(context, keyLoader);
        await stripe.confirmPayment(paymentIntentId, paymentMethodId);
        await CryptoUser.instance.increaseCryptoWalletBalance(tickerData.id, cryptoUnitCount);
        Navigator.of(keyLoader.currentContext,rootNavigator: true).pop();
        await Dialogs.getAlertBox(
            context,
            'Success',
            '$cryptoUnitCount ${tickerData.id} added to your crypto wallet',
            'Go to Dashboard'
        );
        Navigator.of(context).popUntil((route) => route.isFirst);
      }
      catch(e) {
        Navigator.of(keyLoader.currentContext,rootNavigator: true).pop();
        Dialogs.getAlertBox(context, 'Failed to process payment', e.toString() ?? 'Empty error message');
      }
    };

    var onClickPurchase = (String paymentAmount, String paymentMethodId) async {
      Stripe stripe = Stripe.instance;
      try {
        String adjustedPaymentToTwoDecimals = (double.parse(paymentAmount)).toStringAsFixed(2);
        int paymentInCents = (double.parse(adjustedPaymentToTwoDecimals)*100).floor();
        Dialogs.showLoadingDialog(context, keyLoader);
        Map<String, String> metaData = {
          'cryptoCurrency': tickerData.id,
          'coinCount': cryptoUnitCount
        };
        Map<String, dynamic> response = await stripe.createPaymentIntent(paymentInCents, paymentMethodId, metaData);
        Navigator.of(keyLoader.currentContext,rootNavigator: true).pop();
        ConfirmationStatus status =  await Dialogs.getConfirmationAlertBox(context,
            'Confirmation',
            'Do you confirm this payment?',
        );
        if(status == ConfirmationStatus.CONFIRMED) {
          confirmPayment(response['id'], paymentMethodId);
        }
      }
      catch(e) {
        Dialogs.getAlertBox(context, 'Failed', e.toString() ?? 'Empty error message');
      }
    };

    return getRaisedButton(
      'Purchase',
      () {
        onClickPurchase(paymentAmount, paymentMethod.paymentMethodId);
      }
    );
  }
}

class PurchasedCryptoCount extends StatelessWidget {

  final String paymentAmount;

  PurchasedCryptoCount({
    Key key,
    @required this.paymentAmount
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    CryptoTickerData tickerData = cryptoTickerDataProvider.getAllCryptos()[0];

    String cryptoUnitCount = getNumberCryptoUnitsForPayment(paymentAmount, tickerData.priceData.price);

    return Text(
      '$cryptoUnitCount ${tickerData.id}',
      style: TextStyle(
          color: Colors.black87,
          fontSize: 40
      ),
    );
  }
}

class PurchaseSummary extends StatelessWidget {
  final PaymentMethod paymentMethod;
  final String paymentAmount;

  PurchaseSummary({
    Key key,
    @required this.paymentAmount,
    @required this.paymentMethod
  });

  @override
  Widget build(BuildContext context) {
    final cryptoTickerDataProvider = Provider.of<CryptoTickerDataModel>(context, listen: false);
    CryptoTickerData tickerData = cryptoTickerDataProvider.getAllCryptos()[0];
    double containerHeight = MediaQuery.of(context).size.height/15;
    const textStyle = TextStyle(
        color: Colors.black87,
        fontSize: 20
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          height: containerHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${tickerData.id} price per unit',
                style: textStyle,
              ),
              Text(
                adjustPrice(tickerData.priceData.price),
                style: textStyle,
              )
            ],
          ),
        ),
        Container(
          height: containerHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Payable amount',
                style: textStyle,
              ),
              Text(
                '\$$paymentAmount',
                style: textStyle,
              )
            ],
          ),
        ),
        Container(
          height: containerHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Payment via',
                style: textStyle
              ),
              Text(
                'Card ending with ${paymentMethod.card.lastFourDigits}',
                style: textStyle,
              )
            ],
          ),
        )
      ],
    );
  }
}